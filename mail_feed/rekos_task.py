
import json, re, os
import easyimap, pyexcel, requests
import openpyxl
from mail_feed.utils import get_stocks, replace_bytes_imap, decode_mime_words
from mail_feed.config import DOWNLOADED_FEEDS, CLEANED_FEEDS, REFRESH_RESULT_EXCEL_PATH
import datetime
from fake_useragent import UserAgent
from requests.packages.urllib3.exceptions import InsecureRequestWarning

####################################### REKOS PART ####################################################


def rekos_parse_excel_feeds() -> dict:
    """
    Функция парсит excel фиды и формирует из них файл rekos.json с остатками по артикулу,
    Возвращает dict, который перед этим записал в файл
    """

    rekos_total_items = []

    rekos_total_items.extend(get_stocks(path='http://driada-sport.ru/data/files/driada-price.xlsx',
                                        fn='driada',
                                        ff='.xlsx',
                                        first_row=14,
                                        col_id='A',
                                        col_name='F',
                                        col_stock='AG',
                                        stock_conv={'более 10': 11}))

    rekos_total_items.extend(get_stocks(path='https://neotren.ru/docs/Price_Catalog/Price_Neotren.xls',
                                        fn='neotren',
                                        ff='.xls',
                                        first_row=21,
                                        col_id='B',
                                        col_name='G',
                                        col_stock='V',
                                        stock_conv={'В наличии': 5, }))

    # rekos_total_items.extend(get_stocks(path='https://neotren.ru/giant/webdata/Price_Dewolf.xls',
    #                                     fn='dewolf',
    #                                     ff='.xls',
    #                                     first_row=9,
    #                                     col_id='A',
    #                                     col_name='F',
    #                                     col_stock='S',
    #                                     stock_conv={'В наличии': 5, }))
    #
    # rekos_total_items.extend(get_stocks(path='https://neotren.ru/giant/webdata/Price_Giant.xls',
    #                                     fn='giant',
    #                                     ff='.xls',
    #                                     first_row=11,
    #                                     col_id='A',
    #                                     col_name='E',
    #                                     col_stock='S',
    #                                     stock_conv={'В наличии': 5, }))

    rekos_dict = {item[0]: item[1] for item in rekos_total_items}

    with open(CLEANED_FEEDS + 'rekos.json', 'w') as json_file:
        json.dump(rekos_dict, json_file)

    return rekos_dict


def is_integer(value) -> bool:
    """ Check for value is_number """
    try:
        int(value)
        return True
    except:
        return False


def stock_value(value) -> int:
    """
    Returns stock value in integer from value in feed
    Transform + to 11 and str to int
    """
    if value == "+":
        return 11
    else:
        try:
            return int(value)
        except:
            return 0


def parse_feed_from_rekos_sk_email(path_to_excel_file:str) -> dict:
    """ Function returns dict with offer_id and stock """
    wb = openpyxl.load_workbook(path_to_excel_file)
    ws = wb.active
    max_row = ws.max_row
    stocks_dict = {}
    for r in range(1, max_row):
        offer_id = ws.cell(r, 1).value
        stock = ws.cell(r, 12).value
        if is_integer(stock):
            stocks_dict[offer_id] = stock_value(stock)
    return stocks_dict


def get_excel_feeds_from_rekos_email(*args):
    """
    Function returns list of filepath to downloaded Excel_feeds from email rekos@sellecom.ru
    :param args: emails list.
    :return:
    """
    imapper = easyimap.connect('imap.yandex.ru', 'rekos@sellecom.ru', 'rekossellecom')
    last_limit_emails = imapper.listup(limit=10)
    imapper.quit()

    from_addrs = ", ".join(args)
    checked_from_addrs = ''

    filepath_list = []
    for email in last_limit_emails:
        re_email = re.findall(r'[\w\.-]+@[\w\.-]+(?:\.[\w]+)+', email.from_addr)[0]
        if re_email in from_addrs and re_email not in checked_from_addrs:
            if email.attachments:

                i = 1
                for attachment in email.attachments:
                    if i == 3:
                        raise AssertionError(
                            "Looks like we have more than 2 attachments and it's gonna break workflow!")
                    if 'xls' in decode_mime_words(attachment[0]):
                        with open(f'{DOWNLOADED_FEEDS}{re_email}{i}.xls', 'wb') as downloaded_feed:
                            downloaded_feed.write(attachment[1])
                        filepath_list.append(f'{DOWNLOADED_FEEDS}{re_email}{i}.xls')
                        i += 1
                checked_from_addrs += re_email
    return filepath_list

def get_gooses_list(client_id, api_key):
    offer_ids = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
              "page": page_number,
              "page_size": 1000
            }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)

    total = json.loads(r.content.decode())["result"]["total"]

    for page_number in range(int(total/100)+1):
        data = {
                  "page": page_number + 1,
                  "page_size": 1000
                }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        for goose in gooses:
            offer_ids.append(goose["offer_id"])
    return offer_ids


def update_stocks_in_ozon(client_id, api_key, stocks_dict):
    URL = "http://api-seller.ozon.ru/v1/product/import/stocks"
    HEADERS = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    stocks = []
    for key in stocks_dict:
        stocks.append({
            "offer_id": key,
            "stock": stocks_dict[key]
        })

    length = len(stocks)
    items, chunk = stocks, 100
    split_stocks = list(zip(*[iter(items)] * chunk))
    remains_stocks = stocks[length-length % 100 : length]
    responses = []
    # Отправляем основную пачку заказов.
    for stock_list in split_stocks:
        for_send_stocks = list(stock_list)
        data_for_send = {
            "stocks" : for_send_stocks
        }
        response = requests.post(url=URL, json=data_for_send, headers=HEADERS)
        responses.append(response)
    # Добиваем остатки
    data_for_send = {
        "stocks": remains_stocks
    }
    response = requests.post(url=URL, json=data_for_send, headers=HEADERS)
    responses.append(response)
    return responses

def update_errors_handler(responses_list):
    wb = openpyxl.Workbook()
    ws = wb.active
    r = 1
    for response in responses_list:
        text = response.text
        result = json.loads(text)["result"]
        for product in result:
            ws.cell(r, 1).value = str(product["offer_id"])
            ws.cell(r, 2).value = str(product["updated"])
            ws.cell(r, 3).value = str(product["errors"])
            r += 1
    wb.save(REFRESH_RESULT_EXCEL_PATH)
    return REFRESH_RESULT_EXCEL_PATH

def rekos_task():
    """

    """

    #filepath_list = get_excel_feeds_from_rekos_email('rekos-sk@mail.ru')
    #wb = pyexcel.load_book(filepath_list[0])
    #wb.save_as(filepath_list[0] + "x")
    #stocks_dict_from_email = parse_feed_from_rekos_sk_email(filepath_list[0] + "x")

    general_stocks_dict = rekos_parse_excel_feeds()
    cur_prods_shop_1 = get_gooses_list("28461", "8b6d35fc-f50c-414a-a47e-24f3c1196d86")
    cur_prods_shop_2 = get_gooses_list("74340", "852debfe-1431-41f0-992e-a7a1322c6600")

    stocks_dict_1 = {prod: 0 for prod in cur_prods_shop_1}
    stocks_dict_2 = {prod: 0 for prod in cur_prods_shop_2}

    # For set null comment or remove next For
    for stock in general_stocks_dict:
        stocks_dict_1[stock] = general_stocks_dict[stock]
        stocks_dict_2[stock] = general_stocks_dict[stock]
    ##
    # stocks_dict.update(stocks_dict_from_email)
    for key, value in stocks_dict_1.items():
        if value < 10:
            stocks_dict_1[key] = 0
    for key, value in stocks_dict_2.items():
        if value < 10:
            stocks_dict_2[key] = 0
    responses_list = update_stocks_in_ozon("28461", "8b6d35fc-f50c-414a-a47e-24f3c1196d86", stocks_dict_1)
    responses_list += update_stocks_in_ozon("74340", "852debfe-1431-41f0-992e-a7a1322c6600", stocks_dict_2)
    update_errors_handler(responses_list)

def gt(dt_str):
    dt, _, us = dt_str.partition(".")
    dt = datetime.datetime.strptime(dt, "%Y-%m-%dT%H:%M:%S")
    us = int(us.rstrip("Z"), 10)
    return dt + datetime.timedelta(microseconds=us)

def delete_files_older_than(mins: int, folder_path):
    if not os.path.exists(folder_path):
        os.mkdir(folder_path)
    files = os.listdir(folder_path)
    for file in files:
        dts = re.findall(r'.*?--(.*?).xls', file)
        if dts == 0:
            continue
        dt = gt(dts[0])
        now = datetime.datetime.now()
        delta = now - dt
        delta_mins = delta.total_seconds()/60
        if delta_mins > mins:
            filepath = os.path.join(folder_path, file)
            os.remove(filepath)


def save_excel_history(xlsx_url, filename, file_extension, folder_path):
    if not os.path.exists(folder_path):
        os.mkdir(folder_path)
    new_name = f"{filename}--{datetime.datetime.now().isoformat()}.{file_extension}"
    file_path = os.path.join(folder_path, new_name)

    requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
    response = requests.get(xlsx_url,
                            headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.67 Safari/537.36'},
                            verify=False)
    xlsx_bytes = response.content
    with open(file_path, "wb") as f:
        f.write(xlsx_bytes)
    return file_path




if __name__ == '__main__':
    import os
    rekos_task()
