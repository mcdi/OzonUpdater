from services.cron import CronTask
import datetime
import time
import mail_feed.tasks as ivan_tasks
from mail_feed.rekos_task import rekos_task, save_excel_history, delete_files_older_than
from django.conf import settings
import os


class AnytosTask(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        ivan_tasks.anytos_task()


class BuisnesMediaTask1(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        ivan_tasks.biznes_media_task("19392", "800c4994-af65-4908-8d89-3ac643cb3601", "biznes-media.pdf")


class BuisnesMediaTask2(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        ivan_tasks.biznes_media_task("46414", "8bd2e532-f6da-49b7-a622-efbc9f641b7b", "biznes-media_2.pdf")


class BuisnesMediaTask3(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        ivan_tasks.biznes_media_task("97305", "6b8fd8cf-c6d3-47ad-8f08-0c901f6d726e", "biznes-media_3.pdf")


class FidikPdfTask(CronTask):
    RUN_EVERY_MINS = 1440
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        ivan_tasks.biznes_media_task("34008", "4ada53a7-e747-42db-b888-40285d0f308e", "fidik.pdf", days_offset=0)


class NotInTimeException(Exception):
    pass

class RekosTask(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    @property
    def in_time(self):
        now_date = datetime.datetime.now()
        now_time = (now_date + datetime.timedelta(hours=3)).time()
        weekday = now_date.isoweekday()
        start_time = datetime.time(hour=8, minute=0, second=0)
        finish_time = datetime.time(hour=13, minute=0, second=0)
        if weekday != 5 and weekday != 6:
            return True

        if weekday == 5 and now_time < start_time:
            return True

        if weekday == 6 and now_time > finish_time:
            return True

        return False

    # Your custom code
    def do(self):
        delete_files_older_than(2880, os.path.join(settings.BASE_DIR, 'mail_feed/cleaned_feeds/xlsx_history/driada-price'))
        delete_files_older_than(2880, os.path.join(settings.BASE_DIR, 'mail_feed/cleaned_feeds/xlsx_history/Price_Neotren'))
        save_excel_history('http://driada-sport.ru/data/files/driada-price.xlsx', 'driada-price', 'xlsx',
                           os.path.join(settings.BASE_DIR, 'mail_feed/cleaned_feeds/xlsx_history/driada-price'))
        save_excel_history('https://neotren.ru/docs/Price_Catalog/Price_Neotren.xls', 'Price_Neotren', 'xls',
                           os.path.join(settings.BASE_DIR, 'mail_feed/cleaned_feeds/xlsx_history/Price_Neotren'))
        if self.in_time:
            rekos_task()
        else:
            #rekos_task()
            raise NotInTimeException("RekosTask")
