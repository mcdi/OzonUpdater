import os

DOWNLOADED_FEEDS = os.environ["BASE_DIR"] + "/" + 'mail_feed/downloaded_feeds/'
CLEANED_FEEDS = os.environ["BASE_DIR"] + "/" + 'mail_feed/cleaned_feeds/'
REFRESH_RESULT_EXCEL_PATH = os.environ["BASE_DIR"] + "/" + "mail_feed/rekos_update_result.xlsx"
# DOWNLOADED_FEEDS = "/Users/kirill/my-projects/OzonUpdater/mail_feed/downloaded_feeds/"
# CLEANED_FEEDS = "/Users/kirill/my-projects/OzonUpdater/mail_feed/cleaned_feeds/"
# REFRESH_RESULT_EXCEL_PATH = "/Users/kirill/my-projects/OzonUpdater/mail_feed/rekos_update_result.xlsx"