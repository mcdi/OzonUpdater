from __future__ import absolute_import, unicode_literals

import csv, datetime, io, json, re, os, time
import easyimap, pyexcel, pytz, requests

from datetime import timedelta
from imapclient import imap_utf7
from fake_useragent import UserAgent
from openpyxl import load_workbook
from PyPDF2 import PdfFileReader, PdfFileWriter
import openpyxl
from mail_feed.config import DOWNLOADED_FEEDS, CLEANED_FEEDS
from mail_feed.utils import get_stocks, replace_bytes_imap, decode_mime_words







# @periodic_task(run_every=(timedelta(minutes=60)), name='anytos_task')
def anytos_task():
    """Таск, который забирает указанные письма с почтового ящика и конвертирует их в CSV.

    На примере этого таска можно посмотреть, как работают функции для UTF-8 преобразований.  
    """

    from_addr = 'sale1@anytos.ru'
    mail_fold = 'Anytos.ru / ТД АНИТОС'

    mail_fold_enc = imap_utf7.encode(mail_fold)
    mail_fold_enc = replace_bytes_imap(mail_fold_enc)

    imapper = easyimap.connect('imap.yandex.ru', 'b2b.ecom@unekon.com', '2295489b', mail_fold_enc)
    last_limit_emails = imapper.listup(limit=100)
    imapper.quit()

    attachments = False
    for email in last_limit_emails:
        if from_addr in email.from_addr:
            if email.attachments:

                if 'ПРАЙС' in decode_mime_words(email.attachments[0][0])[:5].upper():

                    with open(DOWNLOADED_FEEDS + 'anytos.xlsx', 'wb') as downloaded_feed:
                        downloaded_feed.write(email.attachments[0][1])
                    attachments = True
                    break

    if attachments is False:
        return

    wb = load_workbook(filename=DOWNLOADED_FEEDS + 'anytos.xlsx')
    main_sheet = wb['TDSheet']

    items = []  # [(offer_id, stock), ...]
    fails = 0
    curr_pos = 13

    while True:
        if main_sheet[f'A{curr_pos}'].value is None:
            curr_pos += 1
            fails += 1
            if fails == 10:
                break
            else:
                continue

        fails = 0
        offer_id = main_sheet[f'A{curr_pos}'].value
        stock = 0 if main_sheet[f'H{curr_pos}'].value is None else main_sheet[f'H{curr_pos}'].value
        items.append((offer_id, stock))
        curr_pos += 1

    with open(CLEANED_FEEDS + 'anytos.csv', 'w', newline='', encoding="utf-8") as file_csv:
        writer = csv.writer(file_csv, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['offer_id', 'stock'])
        for item in items:
            writer.writerow([item[0], item[1]])


def biznes_media_task(client_id:str, api_key:str, file_name:str, days_offset: int = 1):
    """Таск штучно собирает этикетки с Озона, которые отгрузят завтра, и клеит их в один PDF, правильно сортируя."""
    HEADERS = {'Client-Id': client_id, 'Api-Key': api_key}
    FBS_LIST = {'dir': 'asc', 'filter': {'status': 'awaiting_deliver'}, 'limit': 1000, 'offset': 0}
    FBS_LABEL = {'posting_number': []}

    result = []  # All items with the status 'awaiting_deliver'.
    while True:
        url = 'https://api-seller.ozon.ru/v2/posting/fbs/list'
        r = requests.post(url, headers=HEADERS, json=FBS_LIST)
        if r.status_code == 200:
            result.extend(r.json()['result'])
            if len(r.json()['result']) == 1000:
                FBS_LIST['offset'] += 1000
                continue
            else:
                break
        raise AssertionError(f'{url} ({r.status_code}: {str(r.json())})')


    order_id_int, order_id_str = [], []
    for item in result:
        if item['products'][0]['offer_id'].isdigit():
            order_id_int.append(item)
        else:
            order_id_str.append(item)

    order_id_int =  sorted(order_id_int, key=lambda item: int(item['products'][0]['offer_id']))
    order_id_str =  sorted(order_id_str, key=lambda item: item['products'][0]['offer_id'])
    sorted_result = order_id_int + order_id_str

    tomorrow_date_msk = (datetime.datetime.now(pytz.timezone('Europe/Moscow')) + timedelta(days=days_offset)).strftime('%Y-%m-%d')
    FBS_LABEL['posting_number'] = [item['posting_number'] for item in sorted_result if tomorrow_date_msk in item['shipment_date']]


    pdfs_bytes = []  # Every item is a BytesIO object which represent a single PDF file.
    posting_number = FBS_LABEL['posting_number']
    while True:
        FBS_LABEL['posting_number'] = posting_number[:20]
        if FBS_LABEL['posting_number']:
            url = 'https://api-seller.ozon.ru/v2/posting/fbs/package-label'
            for _ in range(10):
                r = requests.post(url, headers=HEADERS, json=FBS_LABEL)
                if r.status_code == 200:
                    pdfs_bytes.append(io.BytesIO(r.content))
                    del posting_number[:20]
                    break
                else:
                    time.sleep(5)
                    continue
                raise AssertionError(f'{url} ({r.status_code}: {str(r.json())})')
        else:
            break


    writer = PdfFileWriter()
    for reader in map(PdfFileReader, pdfs_bytes):
        for n in range(reader.getNumPages()):
            writer.addPage(reader.getPage(n))

    with open(CLEANED_FEEDS + file_name, 'wb') as pdf_file:
        writer.write(pdf_file)



if __name__ == '__main__':
    anytos_task()
