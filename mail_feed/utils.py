from __future__ import absolute_import, unicode_literals

import  pyexcel, requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from fake_useragent import UserAgent
from openpyxl import load_workbook
from mail_feed.config import DOWNLOADED_FEEDS, CLEANED_FEEDS
import email as email_lib


def get_stocks(path, fn, ff, first_row, col_id, col_name, col_stock, stock_conv, sheet='TDSheet'):
    """Функция используется для парсинга XLS и XLSX документов.

    Нужно передать: путь к файлу, имя файла, формат файла. Также нужно указать
    первый ряд, с которого начинается список товаров, номера колонок с артикулом, именем и кол-вом.
    По умолчанию (и чаще всего) основной лист называется TDSheet, если иное, то передать его.
    Аргумент stock_conv -- это словарь, куда передаются неоднозначные значения поля остатков.
    Допустим, если в остатках указано "наличие больше 5", то передаем такой словарь {'наличие больше 5': 6}.

    :returns [(offer_id, stock), ...]
    """

    if 'http' in path:
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        r = requests.get(path, headers={'User-Agent': UserAgent().chrome}, verify=False)
        if r.status_code == 200:
            with open(DOWNLOADED_FEEDS + fn + ff, 'wb') as file:
                file.write(r.content)
        else:
            raise AssertionError(f'{path} ({r.status_code})')

    if 'xls' in path:
        pyexcel.Sheet(
            pyexcel.get_sheet(file_name=DOWNLOADED_FEEDS + fn + ff, name_columns_by_row=0).to_array()).save_as(
            DOWNLOADED_FEEDS + fn + '.xlsx')
        sheet = 'pyexcel sheet'

    main_sheet = load_workbook(DOWNLOADED_FEEDS + fn + '.xlsx')[sheet]
    items = []

    fails = 0
    first_row -= 1
    while True:
        first_row += 1
        if main_sheet[f'{col_id}{first_row}'].value and main_sheet[f'{col_name}{first_row}'].value:
            offer_id = main_sheet[f'{col_id}{first_row}'].value
            stock = main_sheet[f'{col_stock}{first_row}'].value if main_sheet[f'{col_stock}{first_row}'].value else 0
            if not str(stock).isdigit():
                stock = stock_conv.get(stock, 0)
            items.append((offer_id, int(stock)))
            fails = 0

        else:
            fails += 1
            if fails == 10:
                break
            continue
    return items


def replace_bytes_imap(byte_string):
    """Функция используется для UTF-8 преобразований, которые понимает протокол SMTP (примеры ниже)."""
    byte_string = byte_string.replace(b'\\', b'\\\\')
    byte_string = byte_string.replace(b'"', b'\\"')
    _var = b'"'
    return _var + byte_string + _var


def decode_mime_words(s):
    """Функция используется для UTF-8 преобразований, которые понимает протокол SMTP (примеры ниже)."""
    return ''.join(
        word.decode(encoding or 'utf8') if isinstance(word, bytes) else word
        for word, encoding in email_lib.header.decode_header(s))