# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import json
import requests
import os
from .refresh_av import get_gooses_list

xml_url = "https://vroznicu.kolyaskyoptom.ru/index.php?route=feed/xmlpriceozonall"
client_id = "19157"
api_key = "785607cb-2931-4847-ab43-0dc3fd970eaa"


file = requests.get(url=xml_url)
text = file.text.encode('ascii', 'ignore').decode('ascii')
path = os.environ["BASE_DIR"] + "/" + "kolyaski_optom/input_xml.xml"
with open(path, "w+") as xml_file:
    xml_file.write(text)


datas = []
tree = ET.parse(path)
root = tree.getroot()
children = root.findall('shop/offers/offer')
for elem in children:
    data = {}
    data['id'] = elem.findall('vendorCode')[0].text
    data['price'] = elem.findall('price')[0].text
    data['stock'] = elem.findall('quantity')[0].text
    datas.append(data)

# Модифицируем полученный массив данных, сопоставляя id с ozon.
# Проблема заключается в том, что в фиде и в Озоне разное кол-во и расположение пробелов.
# Матчим через исключение пробелов.
from_ozon_ids = get_gooses_list(client_id, api_key)
mapping_dict = {}
for ozon_id in from_ozon_ids:
    mapping_dict[ozon_id.replace(" ", "")] = ozon_id

right_data = []
for element in datas:
    element["id"] = element["id"] if element["id"].replace(" ", "") not in mapping_dict else mapping_dict[element["id"]]
    right_data.append(element)

with open(os.environ["BASE_DIR"] + "/" + "kolyaski_optom/datas.json", "w") as f:
    f.write(json.dumps(right_data))
