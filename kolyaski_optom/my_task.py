from services.cron import CronTask
import datetime
import time

class KolyaskiOptomUpdate(CronTask):
    RUN_EVERY_MINS = 15
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        import kolyaski_optom.refresh_json_from_xml
        import kolyaski_optom.refresh_av
        import kolyaski_optom.refresh_pr

