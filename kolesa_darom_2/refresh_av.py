import json
import requests
import os
# Test


json_path = os.environ["BASE_DIR"] + "/" + "kolesa_darom_2/datas.json"
client_id = "44829"
api_key = "32a40d5a-6959-4d76-b347-51db0bed2074"

os.environ["BASE_DIR"] = "/home/kirill/OzonUpdater"
# os.environ["BASE_DIR"] = "/Users/kirill/my-projects/OzonUpdater"
response_path = os.environ["BASE_DIR"] + "/" + "kolesa_darom_2/response_av.json"



def get_gooses_list(client_id, api_key):
    offer_ids = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
        "page": page_number,
        "page_size": 1000
    }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)

    total = json.loads(r.content.decode())["result"]["total"]

    for page_number in range(int(total / 100) + 1):
        data = {
            "page": page_number + 1,
            "page_size": 1000
        }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        for goose in gooses:
            offer_ids.append(goose["offer_id"])
    return offer_ids


def change_available(json_obj, gooses_part, client_id, api_key):
    stocks = []
    for goose in gooses_part:
        f = list(filter(lambda x: x["id"] == str(goose), json_obj))

        if len(f) > 0 and int(f[0]["stock"]) > 0:

            data_template = {
                "offer_id": f[0]["id"],
                "stock": int(f[0]["stock"])
            }

            stocks.append(data_template)
        else:
            data_template = {
                "offer_id": goose,
                "stock": 0
            }
            stocks.append(data_template)

    data = {"stocks": stocks}
    url = "http://api-seller.ozon.ru/v1/product/import/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    return json.dumps(r.content.decode("utf-8"))


def refresh_av(json_path, client_id, api_key):
    with open(json_path, "r+") as json_file:
        json_data = json_file.read()

    json_obj = json.loads(json_data)
    gooses_list = get_gooses_list(client_id, api_key)
    # Разбиваем по val
    val = 100

    e1 = 0 + (len(gooses_list)) % val
    e2 = val + (len(gooses_list)) % val
    responses_list = []
    for i in range(0, len(gooses_list) // val + 1):
        if i == 0:
            response = change_available(json_obj, gooses_list[0:(len(gooses_list)) % val], client_id, api_key)
            responses_list.append(response)
        gooses_part = gooses_list[e1:e2]
        # print(len(gooses_part))
        response = change_available(json_obj, gooses_part, client_id, api_key)
        responses_list.append(response)
        e1 += val
        e2 += val

    return responses_list


resp = refresh_av(json_path, client_id, api_key)
with open(response_path, "w+") as f:
    f.write(json.dumps(resp))

