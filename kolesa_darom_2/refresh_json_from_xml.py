# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import os
import json
import requests

xml_url = "https://feed-new.kolesa-darom.ru/feed/live/moskva/ozon_msk.xml"

file = requests.get(url=xml_url)
text = file.text.encode('ascii', 'ignore').decode('ascii')
path = os.environ["BASE_DIR"] + "/" + "kolesa_darom_2/input_xml.xml"
with open(path, "w+") as xml_file:
    xml_file.write(text)


datas = []

tree = ET.parse(path)
root = tree.getroot()
children = root.findall('shop/offers/offer')
for elem in children:
    data = {}
    data['id'] = elem.attrib['id']
    data['price'] = elem.findall('price')[0].text
    data['stock'] = elem.findall('outlets/outlet')[0].attrib['instock']
    if 'true' in elem.attrib['available'].lower():
    	datas.append(data)
with open(os.environ["BASE_DIR"] + "/" + "kolesa_darom_2/datas.json", "w") as f:
    f.write(json.dumps(datas))
