from services.cron import CronTask
import datetime
import time

class KolesaDarom2Update(CronTask):
    RUN_EVERY_MINS = 15
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        import kolesa_darom_2.refresh_json_from_xml
        import kolesa_darom_2.refresh_av
        import kolesa_darom_2.refresh_pr

