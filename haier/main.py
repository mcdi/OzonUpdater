import requests
import re
from pathlib import Path
import os
import xml.etree.ElementTree as ET
import json
from typing import List


def storage_path(cur_dir):
    """
    Function creates and returns absolute path to storage
    """
    storage_dir = os.path.join(cur_dir, "storage")
    if not os.path.isdir(storage_dir):
        os.mkdir(storage_dir)
    return storage_dir


def get_stocks_dict(csv_url):
    """
    Function returns dict with product stocks from csv, which contains offer id in first column and stock in second...
    { offer_id(str):stock(int), ... }
    """
    response = requests.get(csv_url)
    products_data = re.findall(r'(.*?);(.*?)\r\n', response.text)
    products_dict = {}
    for product in products_data[1:]:
        products_dict[product[0]] = int(product[1])
    return products_dict

def get_products_from_xml(xml_url, path_to_save):

    response = requests.get(xml_url)
    with open(path_to_save, "wb") as f:
        f.write(response.content)

    tree = ET.parse(path_to_save)
    root = tree.getroot()
    products = root.findall(".//offer")
    json_products = []
    for product in products:
        price = float(product.find("price").text)

        params = product.findall("param")
        offer_id = None
        for param in params:
            if param.attrib["name"] == "Артикул":
                offer_id = str(param.text)
                break
        if offer_id:
            json_products.append({
                "offer_id": offer_id,
                "price": price
            })
    return json_products

def data_mapping(price_list:list, stocks_dict:dict):
    price_list_keys = [x["offer_id"] for x in price_list]
    stocks_dict_keys = list(stocks_dict.keys())
    all_keys = list(set(price_list_keys + stocks_dict_keys))

    datas_list = []

    for key in all_keys:
        price_filter = list(filter(lambda x:x["offer_id"] == key, price_list))
        if len(price_filter) > 0:
            price = price_filter[0]
        else:
            price = 0
        stock = stocks_dict.get(key, 0)
        datas_list.append({
            "offer_id": key,
            "stock": stock,
            "price": price
        })

    return datas_list


def get_ozon_offer_ids_list(client_id, api_key) -> List[str]:
    offer_ids = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
              "page": page_number,
              "page_size": 1000
            }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    total = json.loads(r.content.decode())["result"]["total"]
    for page_number in range(int(total/100)+1):
        data = {
                  "page": page_number + 1,
                  "page_size": 1000
                }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        for goose in gooses:
            offer_ids.append(goose["offer_id"])
    return offer_ids

def change_available(datas_part, client_id, api_key):
    data = {"stocks": datas_part}
    url = "http://api-seller.ozon.ru/v1/product/import/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text

def change_price(datas_part, client_id, api_key):
    data = {"prices": datas_part}

    url = "http://api-seller.ozon.ru/v1/product/import/prices"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text

def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list

def get_final_json(ozon_offer_ids, datas_json):
    new_list = []
    for offer_id in ozon_offer_ids:
        datas_filter = list(filter(lambda x:x["offer_id"] == offer_id, datas_json))
        if len(datas_filter) == 0:
            new_list.append({
                "offer_id": offer_id,
                "price": "0",
                "stock": 0
            })
        else:
            data = datas_filter[0]
            new_list.append({
                "offer_id": offer_id,
                "price": str(data["price"]),
                "stock": data["stock"]
            })
    return new_list


def main():

    OZON_CLIENT_ID = "93900"
    OZON_API_KEY = "8b6f8438-59e7-4c13-8354-d278ee2a5cfe"

    CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
    STORAGE_DIR = storage_path(CUR_DIR)

    stocks_dict = get_stocks_dict("http://хайер-спб.рф/ostatki/ostatki.csv")
    price_list = get_products_from_xml("http://хайер-спб.рф/index.php?route=feed/yml/download&profile_id=25", os.path.join(STORAGE_DIR, "feed.xml"))
    united_json = data_mapping(price_list, stocks_dict)

    with open(os.path.join(STORAGE_DIR, "datas.json"), "w") as f:
        f.write(json.dumps(united_json, ensure_ascii=False))

    ### Unites offer_ids from ozon with current dataset for stock nullifying.
    ozon_offer_ids = get_ozon_offer_ids_list(OZON_CLIENT_ID, OZON_API_KEY)
    final_json = get_final_json(ozon_offer_ids, united_json)

    #### Update stocks in OZON
    chunks = array_division(final_json, 100)
    response = []
    for chunk in chunks:
        response_part = change_available(chunk, OZON_CLIENT_ID, OZON_API_KEY)
        response.append(response_part)

    with open(os.path.join(STORAGE_DIR, "response_av.json"), "w") as f:
        f.write(json.dumps(response, ensure_ascii=False))

    ### Update price in OZON
    chunks = array_division(final_json, 100)
    response = []
    for chunk in chunks:
        response_part = change_price(chunk, OZON_CLIENT_ID, OZON_API_KEY)
        response.append(response_part)

    with open(os.path.join(STORAGE_DIR, "response_pr.json"), "w") as f:
        f.write(json.dumps(response, ensure_ascii=False))



if __name__ == '__main__':
    main()

