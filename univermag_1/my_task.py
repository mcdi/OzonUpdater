from services.cron import CronTask
from .main import *



class Univermag1Update(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh stocks for Univermag 1
        :return: None
        """
        # Constants
        CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
        CLIENT_ID_FROM = "19392"
        API_KEY_FROM = "01745908-85ed-4a5b-a1b0-c834e45e1fd1"
        CLIENT_ID_TO = "97305"
        API_KEY_TO = "898cf34d-88b3-4a3c-af65-8dd148228fdc"
        ###

        storage_dir = storage_path(CUR_DIR)

        final_json = get_ozon_stocks(CLIENT_ID_FROM, API_KEY_FROM)

        save_to_storage(json.dumps(final_json), "datas.json", storage_dir)

        chunks = array_division(final_json, 100)
        response = []
        for chunk in chunks:
            response_part = change_available(chunk, CLIENT_ID_TO, API_KEY_TO)
            response.append(response_part)

        save_to_storage(json.dumps(response), "response_av.json", storage_dir)