import requests
from pathlib import Path
import os
import json


def storage_path(cur_dir):
    """
    Function creates and returns absolute path to storage
    """
    storage_dir = os.path.join(cur_dir, "storage")
    if not os.path.isdir(storage_dir):
        os.mkdir(storage_dir)
    return storage_dir

def save_to_storage(string_data, new_name, storage_path) -> str:
    absolute_path = os.path.join(storage_path, new_name)
    with open(absolute_path, "w+") as f:
        f.write(string_data)
    return absolute_path

def change_available(datas_part, client_id, api_key):
    data = {"stocks": datas_part}
    url = "http://api-seller.ozon.ru/v1/product/import/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text

def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list

def get_ozon_stocks(client_id, api_key):
    stocks = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
              "page": page_number,
              "page_size": 1000
            }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    total = json.loads(r.content.decode())["result"]["total"]
    for page_number in range(int(total/100)+1):
        data = {
                  "page": page_number + 1,
                  "page_size": 1000
                }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        for goose in gooses:
            try:
                stocks.append({
                    "offer_id": goose["offer_id"],
                    "stock": goose["stock"]["present"]
                })
            except:
                stocks.append({
                    "offer_id": goose["offer_id"],
                    "stock": 0
                })
    return stocks