from services.cron import CronTask
from kurazh_mebel.refresh_pr import refresh_pr
from kurazh_mebel.refresh_av import refresh_av
from kurazh_mebel.auth_data import shops_list
from kurazh_mebel.xml_feed import XmlFeed
import os
import json



class KurazhMebelUpdate(CronTask):
    RUN_EVERY_MINS = 30
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks for Kurazh Mebel shop
        :return: None
        """
        json_path = os.environ["BASE_DIR"] + "/" + "kurazh_mebel/datas.json"
        xml_feed_url = "https://kurazh-mebel.ru/upload/acrit.exportproplus/ym_vendor_model_OZON.xml?1596010421"
        xml_feed_path = os.environ["BASE_DIR"] + "/" + "kurazh_mebel/xml_feed.xml"

        x = XmlFeed(xml_feed_url, xml_feed_path)
        js = x.target_dict()
        with open(json_path, "w") as f:
            f.write(json.dumps(js))

        for shop in shops_list:
            response_pr = refresh_pr(json_path, shop["client_id"], shop["api_key"])
            response_av = refresh_av(json_path, shop["client_id"], shop["api_key"])
            with open(os.environ["BASE_DIR"] + "/" + "kurazh_mebel/" + shop["client_id"] + "response_pr.json", "w") as f:
                f.write(json.dumps(response_pr))

            with open(os.environ["BASE_DIR"] + "/" + "kurazh_mebel/" + shop["client_id"] + "response_av.json", "w") as f:
                f.write(json.dumps(response_av))