from itertools import chain
import json
import xml.etree.ElementTree as ET
import requests

# XML namespace
nc = {'m': 'urn:1C.ru:commerceml_205'}


def download(path_to_xml):
    url = "https://1cfresh.com:443/a/sbm/1131094/ws/SiteExchange"

    payload = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:exc=\"http://www.1c.ru/ExchangeWithSites\">\n   <soap:Header/>\n   <soap:Body>\n      <exc:GetAmountAndPrices>\n         <exc:ModificationDate>0001-01-01</exc:ModificationDate>\n         <exc:GroupCode></exc:GroupCode>\n         <exc:WarehouseCode></exc:WarehouseCode>\n         <exc:OrganizationCode></exc:OrganizationCode>\n      </exc:GetAmountAndPrices>\n   </soap:Body>\n</soap:Envelope>"
    headers = {
        'Authorization': 'Basic bGlkZXJNWjpwdTFjZUJp',
        'Content-Type': 'application/xml'
    }

    resp = requests.request("POST", url, headers=headers, data=payload)

    with open(path_to_xml, 'wb') as f:
        f.write(resp.content)


def get_offer_price(offer, price_ids):
    # select nessesary ids
    ozon_id = price_ids['OZON цена']
    retail_id = price_ids['Рознична цена']

    # id -> price
    id2price = lambda x: offer.find(f'.//m:Цена[m:ИдТипаЦены="{x}"]'
                                    '/m:ЦенаЗаЕдиницу', nc)
    if id2price(ozon_id) is not None:
        price = id2price(ozon_id)
    else:
        price = id2price(retail_id)
    try:
        return price.text
    except:
        return 0

def offers_mutation(offers):
    unstock_offer_ids = [
        "112104",
        "245184",
        "484155-01",
        "484155-02",
        "484155-03",
        "484155-04",
        "842067",
        "213982",
        "409409-02",
        "409409-02"
    ]
    for offer in offers:
        if offer["id"] in unstock_offer_ids:
            offer["stock"] = 0
    return offers

def convert(path_to_xml, result_path):
    # loads xml's
    et = ET.parse(path_to_xml)

    # parse prices from first file (dict: name -> id)
    price_ids = {type_.find('m:Наименование', nc).text:
                     type_.find('m:Ид', nc).text
                 for type_ in et.findall('.//m:ТипЦены', nc)}

    # get all offers from first file to list of dicts
    offers = [{
        'id': offer.find('m:Артикул', nc).text,
        'stock': offer.find('m:Количество', nc).text,
        'price': get_offer_price(offer, price_ids),
    } for offer in chain(et.findall('.//m:Предложение[m:ХарактеристикиТовара]', nc),
                         et.findall('.//m:Предложение', nc))]

    offers = offers_mutation(offers)

    # dumps result
    with open(result_path, 'w') as f:
        json.dump(offers, f)
    return offers


def get_from_vendors_base():
    response = requests.get("http://vendors.seller-ecom.ru/api/products/?token=2b6d8342890048056cdd763890782a08cb363726&vendors=54")
    js = response.json()
    total = js["total"]
    if total <= 1:
        products = js["products"]
    else:
        products = js["products"]
        for page in range(1, total):
            response = requests.get(
                f"http://vendors.seller-ecom.ru/api/products/?token=2b6d8342890048056cdd763890782a08cb363726&vendors=54&page={page}")
            products += response.json()["products"]
    return [{
        "offer_id": product["offer_id"],
        "price": product["price"],
        "stock": product["stock"]
    } for product in products]
