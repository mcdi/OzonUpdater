from services.cron import CronTask
from progress_mover.refresh_pr import refresh_pr
from progress_mover.refresh_av import refresh_av
from progress_mover.datas_generator import download, convert, get_from_vendors_base
import os
import json



class ProgressMoverUpdate(CronTask):
    RUN_EVERY_MINS = 30
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks for Forward Bike shop
        :return: None
        """

        CLIENT_ID = "61975"
        API_KEY = "561255b6-7235-4bbe-9e01-c214989e1db8"

        # path_to_xml_1 = os.path.join(os.environ["BASE_DIR"], "progress_mover", "xml1.xml")
        # path_to_xml_2 = os.path.join(os.environ["BASE_DIR"], "progress_mover", "xml2.xml")
        json_path = os.path.join(os.environ["BASE_DIR"], "progress_mover", "datas.json")

        # download(path_to_xml_1)
        # convert(path_to_xml_1, json_path)
        datas = get_from_vendors_base()
        with open(json_path, "w") as f:
            f.write(json.dumps(datas))

        response_pr = refresh_pr(json_path, CLIENT_ID, API_KEY)
        response_av = refresh_av(json_path, CLIENT_ID, API_KEY)
        with open(os.environ["BASE_DIR"] + "/" + "progress_mover/" + CLIENT_ID + "response_pr.json", "w") as f:
            f.write(json.dumps(response_pr))

        with open(os.environ["BASE_DIR"] + "/" + "progress_mover/" + CLIENT_ID + "response_av.json", "w") as f:
            f.write(json.dumps(response_av))