from services.cron import CronTask
from .main import *



class ChesnokovUpdate(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks for Forward Bike shop
        :return: None
        """
        # Constants
        CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
        IMPORT_URL = "http://ovz10.ecomseller.px7zm.vps.myjino.ru/upload/webdata%20-%204974a67a-dc68-11ea-b30c-a1280bd79cb7/import.xml"
        OFFERS_URL = "http://ovz10.ecomseller.px7zm.vps.myjino.ru/upload/webdata%20-%204974a67a-dc68-11ea-b30c-a1280bd79cb7/offers.xml"
        OZON_CLIENT_ID = "79976"
        OZON_API_KEY = "fd128b5e-4d21-40cb-ac76-7d11637e0abb"
        ###

        storage_dir = storage_path(CUR_DIR)
        import_xml_path = download_file(IMPORT_URL, "import.xml", storage_dir)
        offers_xml_path = download_file(OFFERS_URL, "offers.xml", storage_dir)

        xml_create_date = get_import_xml_date(import_xml_path)
        last_update_date = get_date_from_memory(storage_dir)
        if xml_create_date < last_update_date:
            return None

        import_json = import_xml_to_json(import_xml_path)
        offers_json = offers_xml_to_json(offers_xml_path)
        united_json = unite_jsons(import_json, offers_json)

        ozon_offer_ids_list = get_ozon_offer_ids_list(OZON_CLIENT_ID, OZON_API_KEY)
        final_json = get_final_json(united_json, ozon_offer_ids_list)

        save_to_storage(json.dumps(final_json), "datas.json", storage_dir)

        chunks = array_division(final_json, 100)
        response = []
        for chunk in chunks:
            response_part = change_available(chunk, OZON_CLIENT_ID, OZON_API_KEY)
            response.append(response_part)
        save_to_storage(json.dumps(response), "response_av.json", storage_dir)

        delta = datetime.timedelta(hours=3)
        set_date_to_memory(datetime.datetime.now() + delta, storage_dir)