import requests
import xml.etree.ElementTree as ET
from typing import List
from pathlib import Path
import os
import json
import re
import datetime


def set_date_to_memory(date: datetime.datetime, storage_dir:str) -> None:
    memory_file_path = os.path.join(storage_dir, "memory.json")
    date_str = date.strftime("%Y-%m-%dT%H:%M:%S")
    data = {"last_update_date": date_str}
    with open(memory_file_path, "w+") as f:
        f.write(json.dumps(data))

def get_date_from_memory(storage_dir:str) -> datetime.datetime:
    try:
        memory_file_path = os.path.join(storage_dir, "memory.json")
        with open(memory_file_path, "r") as f:
            data = json.loads(f.read())
        date = datetime.datetime.strptime(data["last_update_date"], "%Y-%m-%dT%H:%M:%S")
        return date
    except:
        date = datetime.datetime.strptime("1990-10-20T10:00:00", "%Y-%m-%dT%H:%M:%S")
        return date

def storage_path(cur_dir):
    """
    Function creates and returns absolute path to storage
    """
    storage_dir = os.path.join(cur_dir, "storage")
    if not os.path.isdir(storage_dir):
        os.mkdir(storage_dir)
    return storage_dir

def download_file(url, new_name, storage_path) -> str:
    """
    Function returns absolute path to downloaded file
    """
    response = requests.get(url)
    bytes = response.content
    absolute_path = os.path.join(storage_path, new_name)
    with open(absolute_path, "wb") as f:
        f.write(bytes)
    return absolute_path

def save_to_storage(string_data, new_name, storage_path) -> str:
    absolute_path = os.path.join(storage_path, new_name)
    with open(absolute_path, "w+") as f:
        f.write(string_data)
    return absolute_path

def import_xml_to_json(absolute_path) -> List[dict]:
    """
    Returns [{"id": "***", "offer_id": "***"},...]
    """
    tree = ET.parse(absolute_path)
    root = tree.getroot()
    products = root.findall(".//{urn:1C.ru:commerceml_205}Товар")
    json_products = []
    for product in products:
        id = product.find("{urn:1C.ru:commerceml_205}Ид").text
        offer_id = product.find("{urn:1C.ru:commerceml_205}Артикул").text
        json_products.append({
            "id": id,
            "offer_id": offer_id
        })
    return json_products

def get_import_xml_date(absolute_path):
    tree = ET.parse(absolute_path)
    root = tree.getroot()
    date = root.attrib["ДатаФормирования"]
    date = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    return date

def get_color(et_element) -> str:
    """ Looks for color in product which is et element """
    try:
        chars = et_element.find("{urn:1C.ru:commerceml_205}ХарактеристикиТовара")
        for char in chars:
            if char.find("{urn:1C.ru:commerceml_205}Наименование").text == "Цвет":
                color = char.find("{urn:1C.ru:commerceml_205}Значение").text
                return color
        return ""
    except:
        return ""

def offers_xml_to_json(absolute_path) -> List[dict]:
    """
    Returns [{"id": "***", "color": "***", "stock": **},...]
    """
    tree = ET.parse(absolute_path)
    root = tree.getroot()
    products = root.findall(".//{urn:1C.ru:commerceml_205}Предложение")
    json_products = []
    for product in products:
        id = product.find("{urn:1C.ru:commerceml_205}Ид").text
        id = id.split("#")[0]
        color = get_color(product)
        stock = product.find("{urn:1C.ru:commerceml_205}Количество").text
        json_products.append({
            "id": id,
            "color": color,
            "stock": stock
        })
    return json_products

def unite_jsons(import_json, offers_json) -> List[dict]:
    """
    Function unites couples of products from both of jsons by id.
    :import_json [{"id": "***", "offer_id": "***"},...]
    :offers_json [{"id": "***", "color": "***", "stock": **},...]
    :returns [{"id": "***", "offer_id": "***", "stock": ***, "colors": []}, ... ]
    """
    united_json = []
    for product in import_json:
        filtered_offers = list(filter(lambda x:x["id"]==product["id"], offers_json))
        if len(filtered_offers) == 0:
            continue
        colors_list = [{"color": offer["color"], "stock": int(offer["stock"])} for offer in filtered_offers]
        united_json.append({
            "id": product["id"],
            "offer_id": product["offer_id"],
            "colors": colors_list
        })
    return united_json

def get_ozon_offer_ids_list(client_id, api_key) -> List[str]:
    offer_ids = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
              "page": page_number,
              "page_size": 1000
            }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    total = json.loads(r.content.decode())["result"]["total"]
    for page_number in range(int(total/100)+1):
        data = {
                  "page": page_number + 1,
                  "page_size": 1000
                }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        for goose in gooses:
            offer_ids.append(goose["offer_id"])
    return offer_ids

def get_final_json(united_json, right_offer_ids_list) -> List[dict]:
    """
    :united_json [{"id": "***", "offer_id": "***", "stock": ***, "colors": [{'color': 'Черный', 'stock': 11}, ...]}, ... ]
    :right_offer_ids_list ["***", "***", ...]
    :returns [{"offer_id": "***", "stock": ***, "price": ***}]
    """
    transformed_json = []
    for offer_id in right_offer_ids_list:
        try:
            clean_offer_id = re.findall(r'^(\d*)', offer_id)[0]
        except:
            continue
        united_product_filter = list(filter(lambda x:x["offer_id"]==clean_offer_id, united_json))
        if len(united_product_filter) == 0:
            transformed_json.append({
                "offer_id": offer_id,
                "stock": 0,
                "price": 0
            })
            continue
        else:
            united_product = united_product_filter[0]
            if len(united_product["colors"]) == 1:
                transformed_json.append({
                    "offer_id": offer_id,
                    "stock": united_product["colors"][0]["stock"],
                    "price": 0
                })
            else:
                color_finded = False
                for color in united_product["colors"]:
                    colored_offer_id = f"{clean_offer_id}_{color['color'].lower()}"
                    if offer_id.lower() in colored_offer_id:
                        transformed_json.append({
                            "offer_id": offer_id,
                            "stock": color["stock"],
                            "price": 0
                        })
                        color_finded = True
                        break
                if color_finded == False:
                    transformed_json.append({
                        "offer_id": offer_id,
                        "stock": 0,
                        "price": 0
                    })
    return transformed_json


def change_available(datas_part, client_id, api_key):
    data = {"stocks": datas_part}
    url = "http://api-seller.ozon.ru/v1/product/import/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text

def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list

if __name__ == '__main__':
    # Constants
    CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
    IMPORT_URL = "http://ovz10.ecomseller.px7zm.vps.myjino.ru/upload/webdata%20-%204974a67a-dc68-11ea-b30c-a1280bd79cb7/import.xml"
    OFFERS_URL = "http://ovz10.ecomseller.px7zm.vps.myjino.ru/upload/webdata%20-%204974a67a-dc68-11ea-b30c-a1280bd79cb7/offers.xml"
    OZON_CLIENT_ID = "79976"
    OZON_API_KEY = "fd128b5e-4d21-40cb-ac76-7d11637e0abb"
    ###

    storage_dir = storage_path(CUR_DIR)
    import_xml_path = download_file(IMPORT_URL, "import.xml", storage_dir)
    offers_xml_path = download_file(OFFERS_URL, "offers.xml", storage_dir)

    xml_create_date = get_import_xml_date(import_xml_path)
    last_update_date = get_date_from_memory(storage_dir)
    if xml_create_date < last_update_date:
        raise Exception

    import_json = import_xml_to_json(import_xml_path)
    offers_json = offers_xml_to_json(offers_xml_path)
    united_json = unite_jsons(import_json, offers_json)

    ozon_offer_ids_list = get_ozon_offer_ids_list(OZON_CLIENT_ID, OZON_API_KEY)
    final_json = get_final_json(united_json, ozon_offer_ids_list)

    save_to_storage(json.dumps(final_json), "datas.json", storage_dir)

    chunks = array_division(final_json, 100)
    response = []
    for chunk in chunks:
        response_part = change_available(chunk, OZON_CLIENT_ID, OZON_API_KEY)
        response.append(response_part)
    save_to_storage(json.dumps(response), "response_av.json", storage_dir)

    delta = datetime.timedelta(hours=3)
    set_date_to_memory(datetime.datetime.now() + delta, storage_dir)