from services.cron import CronTask
from forward_bike.refresh_pr import refresh_pr
from forward_bike.refresh_av import refresh_av
from forward_bike.auth_data import shops_list
from forward_bike.xml_feed import XmlFeed
import os
import json



class ForwardBikeUpdate(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks for Forward Bike shop
        :return: None
        """
        json_path = os.environ["BASE_DIR"] + "/" + "forward_bike/datas.json"
        xml_feed_url = "https://forward.bike/bitrix/catalog_export/export_IVL.xml"
        xml_feed_path = os.environ["BASE_DIR"] + "/" + "forward_bike/xml_feed.xml"

        x = XmlFeed(xml_feed_url, xml_feed_path)
        js = x.target_dict()
        with open(json_path, "w") as f:
            f.write(json.dumps(js))

        for shop in shops_list:
            response_pr = refresh_pr(json_path, shop["client_id"], shop["api_key"])
            response_av = refresh_av(json_path, shop["client_id"], shop["api_key"])
            with open(os.environ["BASE_DIR"] + "/" + "forward_bike/" + shop["client_id"] + "response_pr.json", "w") as f:
                f.write(json.dumps(response_pr))

            with open(os.environ["BASE_DIR"] + "/" + "forward_bike/" + shop["client_id"] + "response_av.json", "w") as f:
                f.write(json.dumps(response_av))