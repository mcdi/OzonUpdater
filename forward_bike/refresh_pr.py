import json
import requests



def full_price(price):
    floated = price * 1.15
    main = floated // 10
    return (main + 1) * 10

def change_price(json_part, client_id, api_key):
    prices = []
    for goose in json_part:
        price = float(goose["price"]) if float(goose["price"]) > 750 else 750
        old_price = full_price(price)
        premium_price = (price * 0.95) // 10 * 10
        data_template = {
          "offer_id": str(goose["id"]),
          "price": str(price),
          "old_price": str(old_price),
          "premium_price": str(premium_price)
        }
        prices.append(data_template)

    data = {"prices": prices}
    url = "http://api-seller.ozon.ru/v1/product/import/prices"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    return r.text


def refresh_pr(json_path, client_id, api_key):
    with open(json_path, "r+") as json_file:
        json_data = json_file.read()

    json_obj = json.loads(json_data)
    # Разбиваем по 1000
    div = 500
    e1 = 0 +(len(json_obj))%div
    e2 = div + (len(json_obj))%div

    responses_list = []
    for i in range(0, len(json_obj)//div+1):
        if i == 0:
            response = change_price(json_obj[0:(len(json_obj))%1000], client_id, api_key)
            responses_list.append(response)
        json_part = json_obj[e1:e2]
        response = change_price(json_part, client_id, api_key)
        responses_list.append(response)
        e1 += div
        e2 += div
    return responses_list