from instrumentu.refresh_pr import refresh_pr
from instrumentu.refresh_av import refresh_av
from instrumentu.auth_data import shops_list
from instrumentu.xml_feed import XmlFeed
import os
import json

os.environ["BASE_DIR"] = "/Users/kirill/my-projects/OzonUpdater"


json_path = os.environ["BASE_DIR"] + "/" + "instrumentu/datas.json"
xml_feed_url = "http://new.grand-instrument.ru/bitrix/catalog_export/yandex_ozon.php"
xml_feed_path = os.environ["BASE_DIR"] + "/" + "instrumentu/xml_feed.xml"

x = XmlFeed(xml_feed_url, xml_feed_path)
js = x.target_dict()
with open(json_path, "w") as f:
    f.write(json.dumps(js))


# for shop in shops_list:
shop = shops_list[1]
response_pr = refresh_pr(json_path, shop["client_id"], shop["api_key"])
response_av = refresh_av(json_path, shop["client_id"], shop["api_key"])


with open(os.environ["BASE_DIR"] + "/" + "instrumentu/response_pr.json", "w") as f:
    f.write(json.dumps(response_pr))

with open(os.environ["BASE_DIR"] + "/" + "instrumentu/response_av.json", "w") as f:
    f.write(json.dumps(response_av))