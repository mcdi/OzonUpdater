import os
from pathlib import Path
from services.cron import CronTask

from .storage import UnispartStorage
from .orders import get_orders, transform_orders_to_excel, get_posting_numbers_from_excel
from .ftp import unispart_uploading


class UnispartOrdersUpload(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks for Forward Bike shop
        :return: None
        """
        # Constants
        CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
        STORAGE_DIR = os.path.join(CUR_DIR, "storage")
        storage = UnispartStorage(STORAGE_DIR)

        OZON_CLIENT_ID = "4286"
        OZON_API_KEY = "67ad8b08-76af-4c9e-9bba-061e12e24da9"

        orders = get_orders(OZON_API_KEY, OZON_CLIENT_ID, 1)

        last_excel = storage.get_last_xlsx()
        if last_excel:
            last_postings_list = get_posting_numbers_from_excel(last_excel)
        else:
            last_postings_list = []
        filtered_orders = [order for order in orders if order.posting_number not in last_postings_list]

        excel_dump = transform_orders_to_excel(filtered_orders)
        file_path, file_name = storage.save_xlsx(excel_dump)

        if len(filtered_orders) == 0:
            return

        unispart_uploading(file_path, file_name)

        storage.remove_old()


