import os
import openpyxl
import datetime
from typing import Union


class UnispartStorage:

    SAFE_DAYS = 5

    def __init__(self, storage_dir):
        self.dir = self.__check_path(storage_dir)

    def __check_path(self, path):
        if not os.path.exists(path):
            os.mkdir(path)
        return path

    def save_xlsx(self, openpyxl_workbook:openpyxl.Workbook):
        """ Function saves file in storage path with name, which formatted as d/m/Y H:M"""
        now = datetime.datetime.now()
        now_str = now.strftime("%d-%m-%Y_%H-%M")
        file_name = now_str + ".xlsx"
        file_path = os.path.join(self.dir, file_name)
        openpyxl_workbook.save(file_path)
        return file_path, file_name

    def get_last_xlsx(self) -> Union[openpyxl.Workbook, None]:
        """ Function returns initialized openpyxl workbook"""
        files = self.detailed_files_data
        files.sort(key=lambda x:x["datetime"])
        if len(files) > 0:
            return openpyxl.load_workbook(files[-1]["filepath"])
        else:
            return None

    @property
    def detailed_files_data(self) -> list:
        files = os.listdir(self.dir)
        output_data = []
        for file in files:
            output_data.append({
                "filename": file,
                "filepath": os.path.join(self.dir, file),
                "datetime": datetime.datetime.strptime(file, "%d-%m-%Y_%H-%M.xlsx")
            })
        return output_data

    def remove_old(self):
        files = self.detailed_files_data
        files.sort(key=lambda x: x["datetime"])
        end_data = datetime.datetime.now() - datetime.timedelta(self.SAFE_DAYS)
        for file in files:
            if file["datetime"] < end_data:
                os.remove(file["filepath"])