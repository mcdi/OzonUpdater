from .lib.ozon_api.order.order import OzonOrder
import datetime
import openpyxl


def get_orders(api_key, client_id, hours):
    order_set = OzonOrder(api_key, client_id, False)
    delta = datetime.timedelta(hours=hours)
    now = datetime.datetime.now()
    orders = order_set.list(
        now - delta,
        now,
        "awaiting_packaging"
    )
    return orders


def transform_orders_to_excel(orders):

    wb = openpyxl.Workbook()
    ws = wb.active

    ws.cell(1, 1).value = "Order Number"
    ws.cell(1, 2).value = "Posting Number"
    ws.cell(1, 3).value = "Offer id"
    ws.cell(1, 4).value = "Product id"
    ws.cell(1, 5).value = "Brand"
    ws.cell(1, 6).value = "Quantity"
    ws.cell(1, 7).value = "Price"

    r = 2
    for order in orders:

        products = order.products
        for product in products:
            ws.cell(r, 1).value = order.order_number
            ws.cell(r, 2).value = order.posting_number
            ws.cell(r, 3).value = product.offer_id
            ws.cell(r, 4).value = product.ids.id
            attrs = product.attributes
            brand_filter = list(filter(lambda attr: attr.name == "Бренд", attrs))
            if len(brand_filter) > 0:
                ws.cell(r, 5).value = brand_filter[0].value
            ws.cell(r, 6).value = product.order_quantity
            ws.cell(r, 7).value = product.order_price
            r += 1

    return wb


def get_posting_numbers_from_excel(wb: openpyxl.Workbook):
    ws = wb.active
    max_rows = ws.max_row
    posting_numbers = []
    for row in range(1, max_rows):
        posting_numbers.append(ws.cell(row, 2).value)
    return posting_numbers