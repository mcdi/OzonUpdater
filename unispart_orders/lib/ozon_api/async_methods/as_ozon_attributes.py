import asyncio
import aiohttp
import json

class TooManyErrorsException(Exception):
    pass

def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list


async def get_response(url, data, headers, session):
    request_counter = 0
    while True:
        try:
            async with session.post(url, json=data, headers=headers) as response:
                response = await response.json(encoding="utf-8")
                output = {
                    "category_id": data["category_id"],
                    "result": response["result"]
                }
                return output
        except:
            pass
        request_counter += 1
        if request_counter > 100:
            return {
                        "category_id": data["category_id"],
                        "result": []
                    }
            # raise TooManyErrorsException(data)


async def get_async_datas(url, datas, headers):
    tasks = []
    async with aiohttp.ClientSession() as session:
        for data in datas:
            task = asyncio.create_task(get_response(url, data, headers, session))
            tasks.append(task)
        data = asyncio.gather(*tasks)
        await data
    return data.result()


def get_product_attributes(client_id, api_key, category_ids_list):
    HEADERS = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    URL = "http://api-seller.ozon.ru/v2/category/attribute"

    ALL_DATAS = [{"category_id": str(category_id)} for category_id in category_ids_list if category_id != 0]
    data = []
    for DATAS in array_division(ALL_DATAS, 1):
        response = asyncio.run(get_async_datas(URL, DATAS, HEADERS))
        for r in response:
            data.append(r)
    return data

if __name__ == '__main__':
    pass
    # For Example
    # CLIENT_ID = "60537"
    # API_KEY = "bc0da741-7299-4754-9bb0-6a62c53ee35a"
    # category_ids_list = ["17027900", "17028993", "15621048", "41777463"]
    # result = get_product_attributes(CLIENT_ID, API_KEY, category_ids_list)