import asyncio
import aiohttp
import time
import openpyxl


class AsyncFetchProductInfoError(Exception):
    pass

class StocksRequestError(Exception):
    pass


def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list

class JsonResponseError(Exception):
    pass


async def get_response(url, data, headers, session):
    async with session.post(url, json=data, headers=headers) as response:
        try:
            data = await response.json()
        except:
            raise JsonResponseError(response.text())
    return data


async def get_async_datas(url, datas, headers):
    tasks = []
    async with aiohttp.ClientSession() as session:
        for data in datas:
            task = asyncio.create_task(get_response(url, data, headers, session))
            tasks.append(task)
        data = asyncio.gather(*tasks)
        await data
    return data.result()


def get_stocks(client_id, api_key):
    HEADERS = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    URL = "http://api-seller.ozon.ru/v1/product/info/stocks"
    FIRST_PAGE_DATA = [{"page": 1, "page_size": 1000}]
    response = asyncio.run(get_async_datas(URL, FIRST_PAGE_DATA, HEADERS))
    try:
        total = response[0]["result"]["total"]
    except:
        raise StocksRequestError(response)
    firts_page_items = response[0]["result"]["items"]
    ALL_DATAS = [{"page": page_number,
              "page_size": 1000} for page_number in range(2, total//1000+2)]
    data = firts_page_items
    for DATAS in array_division(ALL_DATAS, 10):
        response = asyncio.run(get_async_datas(URL, DATAS, HEADERS))
        for r in response:
            data += r["result"]["items"]
    return data


def get_product_info_list(client_id, api_key, offer_ids_list):
    HEADERS = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    URL = "http://api-seller.ozon.ru/v2/product/info/list"
    ALL_DATAS = [{"offer_id": offer_ids,
             "product_id": [],
             "sku": []} for offer_ids in array_division(offer_ids_list, 1000) if len(offer_ids) > 0]
    data = []

    for DATAS in array_division(ALL_DATAS, 5):
        counter = 0
        while True:
            response = asyncio.run(get_async_datas(URL, DATAS, HEADERS))
            try:
                d = []
                for r in response:
                    d += r["result"]["items"]
                data += d
                break
            except:
                pass
            if counter > 20:
                break
                # raise AsyncFetchProductInfoError(f"More to 20 errors for product/info/list method")
            counter += 1
    return data



def get_products_characteristics_list(client_id, api_key, offer_ids_list):
    HEADERS = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    URL = "http://api-seller.ozon.ru/v2/products/info/attributes"
    ALL_DATAS = [{
          "filter": {
            "offer_id": offer_ids,
            "product_id": [
            ]
          },
          "page": 0,
          "page_size": 100
        } for offer_ids in array_division(offer_ids_list, 100) if len(offer_ids) > 0]
    data = []

    for DATAS in array_division(ALL_DATAS, 10):
        counter = 0
        while True:
            response = asyncio.run(get_async_datas(URL, DATAS, HEADERS))
            try:
                d = []
                for r in response:
                    d += r["result"]
                data += d
                break
            except:
                if counter > 10:
                    time.sleep(1)
            if counter > 20:
                with open("async_error.txt", "w") as f:
                    f.write(str(response))
                raise AsyncFetchProductInfoError("More than 20 errors for product/info/attributes method")
            counter += 1
    return data

def offer_id_sku_mapper(info_list):
    map_dict = {}
    for product in info_list:
        offer_id = product["offer_id"]
        try:
            sku = product["sources"][0]["sku"]
        except:
            sku = "Not Found"
        map_dict.update({offer_id: sku})
    return map_dict

def generate_excel_with_offer_id_and_sku_mapping(map_dict, file_path):
    wb = openpyxl.Workbook()
    ws = wb.active
    r = 1
    for key in map_dict:
        ws.cell(r, 1).value = key
        ws.cell(r, 2).value = map_dict[key]
        r += 1
    wb.save(file_path)



if __name__ == '__main__':
    pass
    # For Example
    # t_start = time.time()
    CLIENT_ID = "60537"
    API_KEY = "bc0da741-7299-4754-9bb0-6a62c53ee35a"
    data = get_stocks(CLIENT_ID, API_KEY)
    offer_ids_list = [x["offer_id"] for x in data]
    print(len(offer_ids_list))
    info_list = get_product_info_list(CLIENT_ID, API_KEY, offer_ids_list)
    # map_dict = offer_id_sku_mapper(info_list)
    # generate_excel_with_offer_id_and_sku_mapping(map_dict, "60537.xlsx")
    # print(len(info_list))
    # chars_list = get_products_characteristics_list(CLIENT_ID, API_KEY, offer_ids_list)
    # print(len(chars_list))
    # print(time.time()-t_start)


