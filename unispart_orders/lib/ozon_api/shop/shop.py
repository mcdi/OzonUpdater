from typing import List
from methods.ozon.extends.templates import OzonTemplate
from methods.ozon.extends.methods import OzonMethods
from methods.ozon.product.product import OzonProductShort


from methods.ozon.api import Api


class OzonShop(Api, OzonTemplate, OzonMethods):


    products_memory: List[OzonProductShort] = []


    def __init__(self, Api_Key: str, Client_Id: str, fbo: bool):
        super().__jnit__(Api_Key, Client_Id, fbo)


    def products(self, refresh = False) -> List[OzonProductShort]:
        if refresh == True:
            url = "http://api-seller.ozon.ru/v1/product/list"
            # Получаем общее количество страниц
            data = self.stocks_page_data(1)
            response = self.send_request(url, data).content.decode()
            js = self.get_json_from_response(response)
            pages_count = self.stocks_pages_count(js)
            # Забираем все товары и складываем в all_products
            all_products = []
            for page_number in range(1, pages_count+1):
                data = self.stocks_page_data(page_number)
                response = self.send_request(url, data).content.decode()
                js = self.get_json_from_response(response)
                all_products += js["result"]["items"]

            obj_products = []
            for js_product in all_products:
                obj_product = OzonProductShort(Api_Key=self.api_key, Client_Id=self.client_id, fbo=self.fbo)
                obj_product.offer_id = js_product["offer_id"]
                if "product_id" in js_product:
                    obj_product.product_id = js_product["product_id"]
                obj_products.append(obj_product)

            self.products_memory = obj_products
            return self.products_memory
        else:
            return self.products_memory

    @property
    def categories(self):
        products = self.products_memory
        pass


class OzonAct(OzonShop):

    def create(self):
        pass

    def check(self):
        pass

    def download(self):
        pass

