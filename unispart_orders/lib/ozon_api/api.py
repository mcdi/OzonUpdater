import requests
import json


class Api(object):
    def __jnit__(self, Api_Key: str, Client_Id: str, fbo: bool):
        if fbo == True:
            self.fbo = "fbo"
        else:
            self.fbo = "fbs"
        self.api_key = Api_Key
        self.client_id = Client_Id
        self.session = requests.Session()

    @property
    def headers(self):
        headers = {
            "Client-Id": str(self.client_id),
            "Api-Key": str(self.api_key),
            "Content-Type": "application/json"
        }
        return headers

    def send_request(self, url, data):
        response = self.session.post(url=url, data=json.dumps(data), headers=self.headers)
        return response

    def get_json_from_response(self, response: requests.Response):
        try:
            js = json.loads(response.content.decode())
        except:
            try:
                js = json.loads(response)
            except:
                return None
        return js