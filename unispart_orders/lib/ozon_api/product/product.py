from ..extends.templates import OzonTemplate
from .product_sub_classes import *
import time


class OzonProduct(Api):

    order_quantity:int = 0
    order_price:float = 0

    def __init__(self, Api_Key: str, Client_Id: str, fbo: bool, offer_id: str):
        super().__jnit__(Api_Key, Client_Id, fbo)
        self.offer_id = offer_id
        self.info = self._info
        self.characteristics = self._characteristics

        self.name: str = self.info["name"]
        self.barcode: str = self.info["barcode"]
        self.state: str = self.info["state"]
        self.category_id = self.info["category_id"]
        self.created_at: str = self.info["created_at"]

        self.params_detail = self._categories_detail

        # Info SubObjects
        self.visibility_details: OzonProductInfoVisibilityDetail = self._visiblity_details
        self.validation_errors: List[OzonProductInfoValidationError] = []
        self.sources: List[OzonProductInfoSource] = self._sources

        # Characteristics SubObjects
        self.pdf_list: List[OzonProductPDF] = []
        self.attributes: List[OzonProductAttribute] = self._attributes


    def __str__(self):
        return self.name

    @property
    def ids(self) -> Ids:
        return Ids(self.info, self.characteristics)

    @property
    def prices(self) -> Prices:
        return Prices(self.info, self.characteristics)

    @property
    def dimensions(self) -> Dimensions:
        return Dimensions(self.info, self.characteristics)

    @property
    def images(self) -> Images:
        return Images(self.info, self.characteristics)

    @property
    def availables(self) -> Availables:
        return Availables(self.info, self.characteristics)


    @property
    def _info(self):
        url = "http://api-seller.ozon.ru/v1/product/info"
        data = OzonTemplate().product_info_data(offer_id=self.offer_id)
        response = self.send_request(url, data)
        info = json.loads(response.text)["result"]
        return info


    @property
    def _characteristics(self):
        url = "http://api-seller.ozon.ru/v2/products/info/attributes"
        data = OzonTemplate().product_characteristics_data(offer_id=self.offer_id)
        response = self.send_request(url, data)
        info = json.loads(response.text)["result"]
        if len(info) > 0:
            return info[0]
        else:
            return None

    @property
    def _sources(self):
        js = self.info["sources"]
        sources = []
        for source in js:
            s = OzonProductInfoSource()
            s.is_enabled = source["is_enabled"]
            s.sku = source["sku"]
            s.source = source["source"]
            sources.append(s)
        return sources


    @property
    def _visiblity_details(self):
        js = self.info["visibility_details"]
        v = OzonProductInfoVisibilityDetail()
        v.has_price = js.get("has_price", None)
        v.has_stock = js.get("has_stock", None)
        v.active_product = js.get("active_product", None)
        return v

    @property
    def _attributes(self):
        attrubutes = self.characteristics["attributes"]
        attributes_list = []
        for attribute in attrubutes:
            a = OzonProductAttribute(self.api_key, self.client_id, self.fbo, self.params_detail)
            a.attribute_id = attribute["attribute_id"]
            a.complex_id = attribute["complex_id"]
            a.set_name()
            try:
                a.dictionary_value_id = attribute["values"][0]["dictionary_value_id"]
                a.value = attribute["values"][0]["value"]
            except Exception as e:
                pass
            attributes_list.append(a)

        return attributes_list

    @property
    def _categories_detail(self) -> List[dict]:
        url = "http://api-seller.ozon.ru/v2/category/attribute"
        data = {
            "category_id": self.category_id
        }
        time.sleep(0.1)
        response = self.send_request(url, data)
        try:
            js = json.loads(response.text)
            result = js["result"]
            return result
        except:
            return []

    # feed_methods
    def set_stock(self, quantity:int):
        pass

    def set_price(self, price: float):
        pass

    def list(self):
        pass


    def load_from_json(self, js):
        not_exists_keys = []
        for key in js:
            try:
                self.__setattr__(key, js[key])
            except:
                not_exists_keys.append(key)
        return self


class OzonProductShort:

    def __init__(self, Api_Key: str, Client_Id: int, fbo: bool):
        self.api_key = Api_Key
        self.client_id = Client_Id
        self.fbo = fbo
        self.offer_id = ""
        self.product_id = ""
        self.full_memory = None

    @property
    def full(self) -> OzonProduct:
        if self.full_memory is None:
            self.full_memory = OzonProduct(self.api_key, self.client_id, self.fbo, self.offer_id)
            return self.full_memory
        else:
            return self.full_memory

