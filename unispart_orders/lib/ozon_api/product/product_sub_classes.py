from typing import List
from .product_sub_objects import *

class Ids:

    def __init__(self, info, chars):
        self.id: str = info["id"]
        self.offer_id: str = info["offer_id"]
        self.product_id: str = ""
        self.sku: str = info["sku"]


class Prices:

    def __init__(self, info, chars):
        self.price: float = 0 if info["price"] == '' else float(info["price"])
        self.old_price: float = 0 if info["old_price"] == '' else float(info["old_price"])
        self.min_ozon_price: float = 0 if info["min_ozon_price"] == '' else float(info["min_ozon_price"])
        self.marketing_price: float = 0 if info["marketing_price"] == '' else float(info["marketing_price"])
        self.premium_price: float = 0 if info["premium_price"] == '' else float(info["premium_price"])
        self.recommended_price: float = 0 if info["recommended_price"] == '' else float(info["recommended_price"])
        self.buybox_price: float = 0 if info["buybox_price"] == '' else float(info["buybox_price"])
        self.vat: float = 0 if info["vat"] == '' else float(info["vat"])

    @property
    def beru_vat(self):
        return "VAT_" + str(int(self.vat*100))


class Availables:

    def __init__(self, info, chars):
        self.stock: int = info["stock"]
        self.visible: bool = info["visible"]
        self.stocks: OzonProductInfoValidationStock = self._stocks(info)

    def _stocks(self, info):
        stocks = info["stocks"]
        s = OzonProductInfoValidationStock()
        s.present = int(stocks["present"])
        s.reserved = int(stocks["reserved"])
        s.coming = int(stocks["coming"])
        return s


class Dimensions:

    def __init__(self, info, chars):
        self.height: float = float(chars["height"])
        self.depth: float = float(chars["depth"])
        self.width: float = float(chars["width"])
        self.dimension_unit: str = chars["dimension_unit"]
        self.weight: float = float(chars["weight"])
        self.weight_unit: str = chars["weight_unit"]

    @property
    def cm_height(self):
        return round(self.height/10, 3)

    @property
    def cm_depth(self):
        return round(self.depth/10, 3)

    @property
    def cm_width(self):
        return round(self.width/10, 3)

    @property
    def kg_weight(self):
        return round(self.weight / 1000, 3)


class Images:

    def __init__(self, info, characteristics):
        self.images: List[OzonProductImage] = self._images(characteristics)
        self.images_360: List[OzonProductImage360] = self._images_360(characteristics)

    def _images(self, characteristics):
        images = characteristics["images"]
        images_list = []
        for image in images:
            try:
                i = OzonProductImage()
                i.default = image["default"]
                i.filename = image["file_name"]
                i.index = image["index"]
                images_list.append(i)
            except:
                print(image)
        return images_list

    def _images_360(self, characteristics):
        return characteristics["images360"]

