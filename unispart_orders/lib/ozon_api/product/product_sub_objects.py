from ..api import Api
import json

# Subclasses
class OzonProductImage:
    filename = ""
    default: bool = None
    index: int = 0

    def download(self):
        pass

    def resize(self):
        pass

class OzonProductImage360:
    pass


class OzonProductPDF:
    pass


class OzonProductAttribute(Api):
    attribute_id = ""
    complex_id = ""
    dictionary_value_id: str = ""
    value: str = ""

    def __init__(self, Api_Key, Client_Id, fbo, params_detail):
        super().__jnit__(Api_Key, Client_Id, fbo)
        self.params_detail = params_detail
        self.name = ""

    def __str__(self):
        return f"{self.name}:{self.value}"

    def __repr__(self):
        return f"{self.name}:{self.value}"
    
    def set_name(self):
        params = list(filter(lambda x: x["id"] == self.attribute_id, self.params_detail))
        if len(params) > 0:
            self.name = params[0]["name"]
        else:
            pass

class OzonProductComplexAttribute:
    def __str__(self):
        return ""


class OzonProductInfoSource:
    is_enabled:bool = None
    sku: str = ""
    source:str = "" #fbo


class OzonProductInfoVisibilityDetail:
    has_price:bool = None
    has_stock:bool = None
    active_product:bool = None


class OzonProductInfoValidationError:
    pass


class OzonProductInfoValidationStock:
    coming:int = 0
    present:int = 0
    reserved:int = 0