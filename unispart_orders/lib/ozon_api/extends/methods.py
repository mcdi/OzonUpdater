class OzonMethods:
    def stocks_pages_count(self, page_json: dict) -> int:
        try:
            result = page_json["result"]
        except:
            result = page_json
        total = result["total"] // 1000 + 1
        return total