import datetime

class OzonTemplate:

    def stocks_page_data(self, page_number: int) -> dict:
        template = {
            "filter": {"visibility": "ALL"},
            "page": page_number,
            "page_size": 1000
        }
        return template

    def orders_get_data(self, posting_number:str):
        data = {
            "posting_number": posting_number
        }
        return data

    def orders_list_data(self, since:datetime.datetime, to:datetime.datetime, status: str, limit: int, offset: int):
        since = since.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        to = to.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        data = {
            "dir": "asc",
            "filter": {
                "since": since,
                "status": status,
                "to": to
            },
            "limit": limit,
            "offset": offset,
            "with": {
                "barcodes": True
            }
        }
        return data

    def orders_delete_data(self, posting_number):
        data = {
            "cancel_reason_id": 402,
            "cancel_reason_message": "Товар поврежден при перевозке",
            "posting_number": posting_number
        }
        return data

    def product_info_data(self, offer_id:str="", product_id:str="", sku:str="") -> dict:
        data = {}
        if offer_id != "":
            data["offer_id"] = offer_id
        if product_id != "":
            data["product_id"] = product_id
        if sku != "":
            data["sku"] = sku
        return data

    def product_characteristics_data(self, offer_id:str="", product_id:str="") -> dict:
        data = {
            "filter": {
            },
            "page": 0,
            "page_size": 0
        }
        if offer_id != "":
            data["filter"]["offer_id"] = [offer_id]

        if product_id != "":
            data["filter"]["product_id"] = [product_id]

        return data

    def order_report_data(self, posting_number):
        now = datetime.datetime.now()
        delta = datetime.timedelta(days=10000)
        early =  now-delta
        to = now.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        since = early.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        data = {
            "filter": {
                "date": {
                    "from": since,
                    "to": to
                },
                "posting_number": posting_number,
                "transaction_type": "all"
            },
            "page": 1,
            "page_size": 10
        }
        return data
