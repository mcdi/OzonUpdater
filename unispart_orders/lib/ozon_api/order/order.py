from ..api import Api
from ..extends.templates import OzonTemplate
from ..product.product import OzonProduct
from typing import List
import datetime
import json

class OzonOrder(Api, OzonTemplate):
    order_id = None #: 128903259
    order_number = None #: "29750371-0013"
    posting_number = None #: "29750371-0013-1"
    status = None #: "delivering"
    cancel_reason_id = None #: 0
    created_at = None #: "2020-02-27T11:48:08Z",
    in_process_at = None #: "2020-02-27T12:52:11Z",
    shipment_date = None # "2020-03-06T10:00:00Z",
    products: List[OzonProduct] = [] # [{}]
    barcodes: List[dict] = [] # [{}]


    def __init__(self, Api_Key: str, Client_Id: int, fbo: bool):
        super().__jnit__(Api_Key, Client_Id, fbo)

    def __limit_offset_dataset(self, quantity):
        dataset_array = []
        full_pages = quantity // 1000
        for i in range(0, full_pages):
            limit = i*1000 + 1000
            offset = i*1000
            dataset_array.append({"limit": limit, "offset": offset})
        last_limit = quantity
        last_offset = quantity - quantity % 1000
        dataset_array.append({"limit": last_limit, "offset": last_offset})
        return dataset_array

    def __load_from_json(self, js):
        new_order = OzonOrder(self.api_key, self.client_id, self.fbo)
        new_order.products = []
        for key in js:
            if key == "products":
                for product in js["products"]:
                    new_product = OzonProduct(self.api_key, self.client_id, self.fbo, product["offer_id"])
                    new_product.order_quantity = int(product["quantity"])
                    new_product.order_price = float(product["price"])
                    new_order.products.append(new_product.load_from_json(product))
            else:
                new_order.__setattr__(key, js[key])
        return new_order

    def get(self, posting_number: str):
        url = "http://api-seller.ozon.ru/v2/posting/" + self.fbo + "/get"
        data = self.orders_get_data(posting_number)
        response = self.send_request(url, data)
        js = self.get_json_from_response(response)
        try:
            order = js["result"]
            return self.__load_from_json(order)
        except Exception as e:
            return None

    def list(self, since:datetime.datetime, to:datetime.datetime, status:str):
        url = "http://api-seller.ozon.ru/v2/posting/" + self.fbo + "/list"
        dataset = self.__limit_offset_dataset(1000000000)
        all_orders = []
        for lim_off in dataset:
            data = self.orders_list_data(since, to, status, lim_off["limit"], lim_off["offset"])
            response = self.send_request(url, data)
            js = self.get_json_from_response(response)
            try:
                orders = js["result"]
                if len(orders) > 0:
                    all_orders += orders
                else:
                    break
            except:
                break

        return [self.__load_from_json(order) for order in all_orders]

    def delete(self):
        url = "http://api-seller.ozon.ru/v2/posting/" + self.fbo + "/cancel"
        data = self.orders_delete_data(self.posting_number)
        response = self.send_request(url, data)
        return response

    def pack(self):
        pass

    def report(self):
        url = "http://api-seller.ozon.ru/v2/finance/transaction/list"
        data = self.order_report_data(self.posting_number)
        response = self.send_request(url, data)
        reports = json.loads(response.text)["result"]
        f = list(filter(lambda x: x["orderNumber"] == self.posting_number, reports))
        return f



# o = OzonOrder("e4d9b14a-998f-42da-a284-07a442d1148b", 49083, False)
# now = datetime.datetime.now()
# delta = datetime.timedelta(days=1)
#
# orders = o.list(now - delta, now, "")
# print(len(orders))
