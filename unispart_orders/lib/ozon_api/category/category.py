from ..api import Api
from typing import List
from ..exceptions.base import OzonRequestException
import json


class OzonCategory:
    def __init__(self, id:int, name:str="", parent_id:int = 0, children_quantity:int = 0):
        """
        OzonCategory object.
        :param id: ! required parameter !
        :param name: not required
        :param children: not required
        """
        self.id: int = id
        self.name: str = name
        self.parent_id: int = parent_id
        self.children_quantity:int = children_quantity


class OzonCategoryTree(Api):

    __categories_recursion_memory = []
    __current_tree = []
    current_parent = 0
    def __init__(self, Api_Key: str, Client_Id: str, fbo: bool=False):
        super().__jnit__(Api_Key, Client_Id, fbo)


    def json(self, category_id="") -> List[dict]:
        """
        Method returns category_tree in dict format.
        Insert one category_id for little tree for one category, or insert nothing.
        :param args: category_id: str
        :return: dict
        """

        url = "http://api-seller.ozon.ru/v1/category/tree"
        data = {
          "category_id": category_id
        } if category_id != "" else {}

        response = self.send_request(url, data)
        js = self.get_json_from_response(response)
        try:
            categories = js["result"]
            return categories
        except Exception as e:
            raise OzonRequestException(js)
            return [{"error": str(e), "response": str(js)}]


    def __categories_recursion(self):
        try:
            is_children = False
            for category in self.__current_tree:
                if category["children"] != []:
                    children_len = len(category["children"])
                    is_children = True
                    upper_array = []
                    for child in category["children"]:
                        child.update({'parent_id': category["category_id"]})
                        upper_array.append(child)
                    category.update({'children_quantity': children_len})
                    category.update({'children': []})
                    self.__current_tree += upper_array

            if is_children:
                self.__categories_recursion()
        except:
            pass


    def objects(self, category_id="") -> List[OzonCategory]:
        """
        Method returns category_tree in objects list with parent mark.
        Insert one category_id for little tree for one category, or insert nothing.
        :param args: category_id: str
        :return: Categories Objects List
        """
        json_tree = self.json(category_id)
        self.__current_tree = json_tree
        self.__categories_recursion()
        objects_list = []
        for category in self.__current_tree:
            if "parent_id" not in category:
                category.update({"parent_id": 0})
            # category.pop('children', None)
            children_quantity = category["children_quantity"] if "children_quantity" in category else 0
            new_category = OzonCategory(category["category_id"], category["title"], category["parent_id"], children_quantity=children_quantity)
            objects_list.append(new_category)
        return objects_list
