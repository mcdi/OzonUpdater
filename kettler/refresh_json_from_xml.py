# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import os
import json
import requests
from urllib.parse import urlencode
import re
import zipfile
import shutil

def delete_xmlns(path):
    with open(path, 'r', encoding="utf-8") as f:   # Здесь укажите нужный путь к файлу
        text = f.read()

    text = re.sub(r'xmlns=\".*?\"', '', text)

    with open(path, 'w', encoding="utf-8") as f:
        f.write(text)
def unzip(source_filename, dest_dir):
    with zipfile.ZipFile(source_filename) as zf:
        zf.extractall(dest_dir)
# скачать xml из яндекса

base_url = 'https://cloud-api.yandex.net/v1/disk/public/resources/download?'

public_key = 'https://yadi.sk/d/zJ1T4dNgfkmgjw'  # Сюда вписываете ссылку на первый фид
# public_key2 = 'https://yadi.sk/d/L2brWeB7F6bfMg' # Сюда вписываете ссылку на второй фид (С наличием)

# Получаем загрузочную ссылку
final_url = base_url + urlencode(dict(public_key=public_key))
# final_url2 = base_url + urlencode(dict(public_key=public_key2))



for i in range(1, 10):
    try:
        response = requests.get(final_url)
        download_url1 = response.json()
        download_url = download_url1['href']
        break
    except:
        pass

# for i in range(1, 10):
#     try:
#         response2 = requests.get(final_url2)
#         download_url3 = response2.json()
#         download_url2 = download_url3['href']
#         break
#     except:
#         pass


# Загружаем файл и сохраняем его
download_response = requests.get(download_url)
with open(os.environ["BASE_DIR"] + "/" + 'kettler/downloaded_file.zip', 'wb') as f:   # Здесь укажите нужный путь к файлу
    f.write(download_response.content)
unzip(os.environ["BASE_DIR"] + "/" + 'kettler/downloaded_file.zip', os.environ["BASE_DIR"] + "/" + 'kettler')
os.rename(os.environ["BASE_DIR"] + "/" + 'kettler/webdata/import0_1.xml', os.environ["BASE_DIR"] + "/" + 'kettler/downloaded_file.xml')
os.rename(os.environ["BASE_DIR"] + "/" + 'kettler/webdata/offers0_1.xml', os.environ["BASE_DIR"] + "/" + 'kettler/downloaded_file2.xml')
shutil.rmtree(os.environ["BASE_DIR"] + "/" + 'kettler/webdata')
# # Загружаем файл и сохраняем его
# download_response2 = requests.get(download_url2)
# with open(os.environ["BASE_DIR"] + "/" + 'kettler/downloaded_file2.xml', 'wb') as f:   # Здесь укажите нужный путь к файлу
#     f.write(download_response2.content)

delete_xmlns(os.environ["BASE_DIR"] + "/" + "kettler/downloaded_file.xml")
delete_xmlns(os.environ["BASE_DIR"] + "/" + "kettler/downloaded_file2.xml")



#делаем остальное

path = os.environ["BASE_DIR"] + "/" + "kettler/downloaded_file.xml"
path2 = os.environ["BASE_DIR"] + "/" + "kettler/downloaded_file2.xml"

# Создаем словарь наличия
parser = ET.XMLParser(encoding="utf-8")
tree = ET.parse(path2, parser=parser)
root = tree.getroot()
children = root.findall('ИзмененияПакетаПредложений/Предложения/Предложение')
availables = {}
for child in children:
    id = child.findall("Ид")[0].text
    av = child.findall("Количество")[0].text
    availables[id] = av


datas = []
parser = ET.XMLParser(encoding="utf-8")
tree = ET.parse(path, parser=parser)
root = tree.getroot()
children = root.findall('Каталог/Товары/Товар')
for elem in children:
    data = {}
    if (elem.findall('Артикул')[0].text is not None):
        data['id'] = elem.findall('Артикул')[0].text
        data['id'] = re.sub(r'^\s*?', '', data['id'])
        data['id'] = re.sub(r'\s*?$', '', data['id'])
        long_id = elem.findall('Ид')[0].text
        
        try:
            data['stock'] = availables[long_id]
            # print(long_id)
        except:
            data['stock'] = 0
         
    # data['stock'] = elem.findall('outlets/outlet')[0].attrib['instock']
        datas.append(data)

with open(os.environ["BASE_DIR"] + "/" + "kettler/datas.json", "w", encoding="utf-8") as f:
    f.write(json.dumps(datas, ensure_ascii=False))
