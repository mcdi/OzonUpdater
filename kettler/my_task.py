from services.cron import CronTask
import datetime
import time

class KettlerUpdate(CronTask):
    RUN_EVERY_MINS = 15
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        import kettler.refresh_json_from_xml
        import kettler.refresh_av

