from services.cron import CronTask
from .main import *
import datetime


class KettlerNew1cUpdate(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks for Forward Bike shop
        :return: None
        """

        day_number = datetime.datetime.isoweekday(datetime.datetime.now())
        if day_number == 6 or day_number == 7:
            return

        # Constants
        CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
        IMPORT_URL = "https://www.kettlermebel.ru/_upload/1c/import0_1.xml"
        OFFERS_URL = "https://www.kettlermebel.ru/_upload/1c/offers0_1.xml"
        OZON_CLIENT_ID = "17788"
        OZON_API_KEY = "d18f2b37-9fd3-4b0c-bf2a-c115703efab1"
        ###

        storage_dir = storage_path(CUR_DIR)
        import_xml_path = download_file(IMPORT_URL, "import.xml", storage_dir)
        offers_xml_path = download_file(OFFERS_URL, "offers.xml", storage_dir)

        xml_create_date = get_import_xml_date(import_xml_path)
        last_update_date = get_date_from_memory(storage_dir)
        # if xml_create_date < last_update_date:
        #     return None

        import_json = import_xml_to_json(import_xml_path)
        offers_json = offers_xml_to_json(offers_xml_path)
        united_json = unite_jsons(import_json, offers_json)

        ozon_stocks_data = get_ozon_stocks_data(OZON_CLIENT_ID, OZON_API_KEY)
        ozon_offer_ids_list = get_offer_ids_from_stocks_data(ozon_stocks_data)

        reserve_dict = get_reserve_dict_from_stocks_data(ozon_stocks_data)
        save_to_storage(json.dumps(reserve_dict), "reserve_dict.json", storage_dir)

        final_json = get_final_json(united_json, ozon_offer_ids_list)
        final_json = divide_reserve_from_feed_data(final_json, reserve_dict)

        save_to_storage(json.dumps(final_json), "datas.json", storage_dir)

        chunks = array_division(final_json, 100)
        response = []
        for chunk in chunks:
            response_part = change_available(chunk, OZON_CLIENT_ID, OZON_API_KEY)
            response.append(response_part)
        save_to_storage(json.dumps(response), "response_av.json", storage_dir)

        set_date_to_memory(datetime.datetime.now(), storage_dir)