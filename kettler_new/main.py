import requests
import xml.etree.ElementTree as ET
from typing import List
from pathlib import Path
import os
import json
import re
import datetime


def set_date_to_memory(date: datetime.datetime, storage_dir:str) -> None:
    memory_file_path = os.path.join(storage_dir, "memory.json")
    date_str = date.strftime("%Y-%m-%dT%H:%M:%S")
    data = {"last_update_date": date_str}
    with open(memory_file_path, "w+") as f:
        f.write(json.dumps(data))

def get_date_from_memory(storage_dir:str) -> datetime.datetime:
    try:
        memory_file_path = os.path.join(storage_dir, "memory.json")
        with open(memory_file_path, "r") as f:
            data = json.loads(f.read())
        date = datetime.datetime.strptime(data["last_update_date"], "%Y-%m-%dT%H:%M:%S")
        return date
    except:
        date = datetime.datetime.strptime("1990-10-20T10:00:00", "%Y-%m-%dT%H:%M:%S")
        return date

def storage_path(cur_dir):
    """
    Function creates and returns absolute path to storage
    """
    storage_dir = os.path.join(cur_dir, "storage")
    if not os.path.isdir(storage_dir):
        os.mkdir(storage_dir)
    return storage_dir

def download_file(url, new_name, storage_path) -> str:
    """
    Function returns absolute path to downloaded file
    """
    response = requests.get(url)
    bytes = response.content
    absolute_path = os.path.join(storage_path, new_name)
    with open(absolute_path, "wb") as f:
        f.write(bytes)
    return absolute_path

def save_to_storage(string_data, new_name, storage_path) -> str:
    absolute_path = os.path.join(storage_path, new_name)
    with open(absolute_path, "w+") as f:
        f.write(string_data)
    return absolute_path

def import_xml_to_json(absolute_path) -> List[dict]:
    """
    Returns [{"id": "***", "offer_id": "***"},...]
    """
    tree = ET.parse(absolute_path)
    root = tree.getroot()
    products = root.findall(".//{urn:1C.ru:commerceml_2}Товар")
    json_products = []
    for product in products:
        id = product.find("{urn:1C.ru:commerceml_2}Ид").text
        offer_id = product.find("{urn:1C.ru:commerceml_2}Артикул").text
        json_products.append({
            "id": id,
            "offer_id": offer_id
        })
    return json_products

def get_import_xml_date(absolute_path):
    tree = ET.parse(absolute_path)
    root = tree.getroot()
    date = root.attrib["ДатаФормирования"]
    date = datetime.datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    return date

def get_color(et_element) -> str:
    """ Looks for color in product which is et element """
    try:
        chars = et_element.find("{urn:1C.ru:commerceml_2}ХарактеристикиТовара")
        for char in chars:
            if char.find("{urn:1C.ru:commerceml_2}Наименование").text == "Цвет":
                color = char.find("{urn:1C.ru:commerceml_2}Значение").text
                return color
        return ""
    except:
        return ""

def offers_xml_to_json(absolute_path) -> List[dict]:
    """
    Returns [{"id": "***", "color": "***", "stock": **},...]
    """
    tree = ET.parse(absolute_path)
    root = tree.getroot()
    products = root.findall(".//{urn:1C.ru:commerceml_2}Предложение")
    json_products = []
    for product in products:
        id = product.find("{urn:1C.ru:commerceml_2}Ид").text
        id = id.split("#")[0]
        color = get_color(product)
        stock = product.find("{urn:1C.ru:commerceml_2}Количество").text
        json_products.append({
            "id": id,
            "color": color,
            "stock": stock
        })
    return json_products

def unite_jsons(import_json, offers_json) -> List[dict]:
    """
    Function unites couples of products from both of jsons by id.
    :import_json [{"id": "***", "offer_id": "***"},...]
    :offers_json [{"id": "***", "color": "***", "stock": **},...]
    :returns [{"id": "***", "offer_id": "***", "stock": ***, "colors": []}, ... ]
    """
    united_json = []
    for product in import_json:
        filtered_offers = list(filter(lambda x:x["id"]==product["id"], offers_json))
        if len(filtered_offers) == 0:
            continue
        colors_list = [{"color": offer["color"], "stock": int(offer["stock"])} for offer in filtered_offers]
        united_json.append({
            "id": product["id"],
            "offer_id": product["offer_id"],
            "colors": colors_list
        })
    return united_json

def get_ozon_stocks_data(client_id, api_key) -> List[str]:
    stocks_data = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
              "page": page_number,
              "page_size": 1000
            }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    total = json.loads(r.content.decode())["result"]["total"]
    for page_number in range(int(total/1000)+1):
        data = {
                  "page": page_number + 1,
                  "page_size": 1000
                }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        stocks_data += gooses
    return stocks_data

def get_offer_ids_from_stocks_data(stocks_data):
    return [offer["offer_id"] for offer in stocks_data]

def get_reserve_dict_from_stocks_data(stocks_data):
    output_dict = {}
    for offer in stocks_data:
        try:
            output_dict[offer["offer_id"]] = offer["stock"]["reserved"]
        except:
            output_dict[offer["offer_id"]] = 0
    return output_dict

def divide_reserve_from_feed_data(feed_data:list, reserve_dict:dict):
    for offer in feed_data:
        reserve = reserve_dict.get(offer["offer_id"], 0)
        if offer["stock"] >= reserve:
            offer["stock"] -= reserve
        else:
            offer["stock"] = 0
    return feed_data


def remove_space(value: str):
    new_value_var = re.findall(r'(.*?)\s+?$', value)
    try:
        new_value = new_value_var[0]
    except:
        new_value = value
    return new_value

def get_final_json(united_json, right_offer_ids_list) -> List[dict]:
    """
    :united_json [{"id": "***", "offer_id": "***", "stock": ***, "colors": [{'color': 'Черный', 'stock': 11}, ...]}, ... ]
    :right_offer_ids_list ["***", "***", ...]
    :returns [{"offer_id": "***", "stock": ***, "price": ***}]
    """
    transformed_json = []

    for offer_id in right_offer_ids_list:
        united_product_filter = list(filter(lambda x:remove_space(str(x["offer_id"]))==remove_space(str(offer_id)), united_json))
        if len(united_product_filter) == 0:
            transformed_json.append({
                "offer_id": offer_id,
                "stock": 0,
                "price": 0
            })
        else:
            united_product = united_product_filter[0]
            transformed_json.append({
                "offer_id": offer_id,
                "stock": united_product["colors"][0]["stock"] if united_product["colors"][0]["stock"] >= 0 else 0,
                "price": 0
            })


    return transformed_json
#

def change_available(datas_part, client_id, api_key):
    data = {"stocks": datas_part}
    url = "http://api-seller.ozon.ru/v1/product/import/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text

def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list

if __name__ == '__main__':
    pass