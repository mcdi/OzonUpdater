from services.cron import CronTask
from instrumentu.refresh_pr import refresh_pr
from instrumentu.refresh_av import refresh_av
from instrumentu.auth_data import shops_list
from instrumentu.xml_feed import XmlFeed
import os
import json



class InstrumentuUpdate(CronTask):
    RUN_EVERY_MINS = 120
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks in "Instrumentru" and "KbtInstrumentru"
        :return: None
        """
        json_path = os.environ["BASE_DIR"] + "/" + "instrumentu/datas.json"
        xml_feed_url = "https://grand-instrument.ru/bitrix/catalog_export/yandex_ozon.php"
        xml_feed_path = os.environ["BASE_DIR"] + "/" + "instrumentu/xml_feed.xml"

        x = XmlFeed(xml_feed_url, xml_feed_path)
        js = x.target_dict()
        with open(json_path, "w") as f:
            f.write(json.dumps(js))

        for shop in shops_list:
            response_pr = refresh_pr(json_path, shop["client_id"], shop["api_key"])
            response_av = refresh_av(json_path, shop["client_id"], shop["api_key"])
            with open(os.environ["BASE_DIR"] + "/" + "instrumentu/" + shop["client_id"] + "response_pr.json", "w") as f:
                f.write(json.dumps(response_pr))

            with open(os.environ["BASE_DIR"] + "/" + "instrumentu/" + shop["client_id"] + "response_av.json", "w") as f:
                f.write(json.dumps(response_av))