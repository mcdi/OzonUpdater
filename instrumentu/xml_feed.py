import requests
import xml.etree.ElementTree as ET
import json


class XmlFeed():
    def __init__(self, url, feed_path):
        self.url = url
        self.feed_path = feed_path
        self.objects_array = []
        self.target_tag = "offer"

    def download(self):
        try:
            response = requests.get(self.url)
            with open(self.feed_path, "wb") as f:
                f.write(response.content)
            return self.feed_path
        except Exception as e:
            return e

    @property
    def __root(self):
        path = self.download()
        tree = ET.ElementTree(file=path)
        root = tree.getroot()
        return root


    def recursion(self, root):
        for r in root:
            if len(list(r)) > 0:
                data = {
                    "tag": r.tag,
                    "element": r,
                    "len": len(list(r))
                }
                self.objects_array.append(data)
                self.recursion(r)
            else:
                pass


    @property
    def offers(self):
        self.recursion(self.__root)
        f = filter(lambda x: x["tag"] == self.target_tag, self.objects_array)
        offers_list = [x["element"] for x in list(f)]
        return offers_list

    def target_dict(self):
        """
        Dict with target id, available, price.
        Needed for update in Ozon.
        :return:
        """
        target_list = []
        for offer in self.offers:
            price = offer.find("price").text
            stock = offer.find("outlets/outlet").attrib["instock"]
            offer_id = ""
            for param in offer.findall("param"):
                name = param.attrib["name"]
                if name == "Код для сайта":
                    offer_id = param.text
            target_list.append({
                "id": offer_id,
                "price": float(price),
                "stock": int(float(stock))
            })
        return target_list


if __name__ == '__main__':
    pass
