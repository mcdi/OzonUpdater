from services.cron import CronTask
from klimov.src import (
    get_from_vendors_base,
    get_ozon_offer_ids_list,
    get_final_json,
    array_division,
    change_available
)
import os
import json
from pathlib import Path


class KlimovUpdate(CronTask):
    RUN_EVERY_MINS = 60
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = False

    # Your custom code
    def do(self):
        """
        Refresh prices and stocks for Forward Bike shop
        :return: None
        """

        CABINETS = [
            {
                "client_id": "28605",
                "api_key": "8600e0f9-37c6-4fc7-b631-dae0a3d7eefb"
            },
            {
                "client_id": "133754",
                "api_key": "70eeac1a-60bc-4123-9c9f-aaeb71a0d1d0"
            }
        ]
        CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
        DATAS_PATH = os.path.join(CUR_DIR, f"datas.json")

        datas = get_from_vendors_base()
        with open(DATAS_PATH, "w") as f:
            f.write(json.dumps(datas, indent=4))

        for cabinet in CABINETS:
            response_path = os.path.join(CUR_DIR, f"last_response{cabinet['client_id']}.json")
            ozon_offer_ids = get_ozon_offer_ids_list(cabinet['client_id'], cabinet['api_key'])
            final_json = get_final_json(ozon_offer_ids, datas)
            arrays = array_division(final_json, 100)

            responses_list = []
            for array in arrays:
                response = change_available(array, cabinet['client_id'], cabinet['api_key'])
                responses_list.append(response)
            with open(response_path, "w") as f:
                f.write(json.dumps(responses_list, indent=4))

