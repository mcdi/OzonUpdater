import requests
import json
from typing import List


def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list


def change_available(datas_part, client_id, api_key):
    data = {"stocks": datas_part}
    url = "http://api-seller.ozon.ru/v1/product/import/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text


def get_ozon_offer_ids_list(client_id, api_key) -> List[str]:
    offer_ids = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
              "page": page_number,
              "page_size": 1000
            }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    total = json.loads(r.content.decode())["result"]["total"]
    for page_number in range(int(total/100)+1):
        data = {
                  "page": page_number + 1,
                  "page_size": 1000
                }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        for goose in gooses:
            offer_ids.append(goose["offer_id"])
    return offer_ids


def get_final_json(ozon_offer_ids, datas_json):
    new_list = []
    for offer_id in ozon_offer_ids:
        datas_filter = list(filter(lambda x: x["offer_id"] == offer_id, datas_json))
        if len(datas_filter) == 0:
            new_list.append({
                "offer_id": offer_id,
                "price": "0",
                "stock": 0
            })
        else:
            data = datas_filter[0]
            new_list.append({
                "offer_id": offer_id,
                "price": str(data["price"]),
                "stock": data["stock"]
            })
    return new_list


def get_from_vendors_base():
    response = requests.get("http://vendors.seller-ecom.ru/api/products/?token=2b6d8342890048056cdd763890782a08cb363726&vendors=56")
    js = response.json()
    total = js["total"]
    if total <= 1:
        products = js["products"]
    else:
        products = js["products"]
        for page in range(1, total):
            response = requests.get(
                f"http://vendors.seller-ecom.ru/api/products/?token=2b6d8342890048056cdd763890782a08cb363726&vendors=56&page={page}")
            products += response.json()["products"]
    return [{
        "offer_id": product["offer_id"],
        "price": product["price"],
        "stock": product["stock"]
    } for product in products]


if __name__ == '__main__':
    CLIENT_ID = "28605"
    API_KEY = "8600e0f9-37c6-4fc7-b631-dae0a3d7eefb"

    datas = get_from_vendors_base()
    print(len(datas))

    ozon_offer_ids = get_ozon_offer_ids_list(CLIENT_ID, API_KEY)
    print(len(ozon_offer_ids))


    final_json = get_final_json(ozon_offer_ids, datas)
    print(len(final_json))

    arrays = array_division(final_json, 100)

    for array in arrays:
        response = change_available(array, CLIENT_ID, API_KEY)
        print(response)
