# -*- coding: utf-8 -*-

import sys
sys.path.append("/home/kirill/OzonUpdater")

from services.config import APP_NAME

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", APP_NAME + ".settings")
import django
django.setup()
from typing import List
from services.cron import CronTask


_settings = __import__(APP_NAME + ".settings", globals(), locals(), ['BASE_DIR'])
BASE_DIR = _settings.BASE_DIR

os.environ["BASE_DIR"] = BASE_DIR

def return_crons(dir, _task) -> List[CronTask]:
    lst = []
    for key in _task.__dict__:

        _task = __import__(dir + ".my_task", globals(), locals(), [key])
        task_cls = eval("_task." + key)
        if "CronTask" in str(task_cls.__repr__) and "my_task" in str(task_cls):
            lst.append(task_cls)
    return lst


dirs = os.listdir(BASE_DIR)

crons_list = []
for dir in dirs:
    try:
        _task = __import__(dir + ".my_task", globals(), locals(), ["*"])
        crons = return_crons(dir, _task)
        crons_list += crons
    except Exception as e:
        pass

#
for cron in crons_list:
    cron()
