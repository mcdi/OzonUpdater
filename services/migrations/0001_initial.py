# Generated by Django 3.0.7 on 2020-06-19 12:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Log',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('short_error', models.CharField(max_length=2000)),
                ('traceback', models.TextField()),
                ('date_time', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=1000)),
                ('run_every_mins', models.IntegerField(null=True)),
                ('run_at_time', models.CharField(max_length=2000, null=True)),
                ('running_now', models.BooleanField(default=False)),
                ('alert', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='TaskEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('started', models.DateTimeField()),
                ('finished', models.DateTimeField()),
                ('success', models.BooleanField(default=True)),
                ('task', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='services.Task')),
            ],
        ),
    ]
