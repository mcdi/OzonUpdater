from django.db import models
import datetime
from typing import List
import json

class Task(models.Model):
    name = models.CharField(max_length=1000)
    run_every_mins = models.IntegerField(null=True)
    run_at_time = models.CharField(max_length=2000, null=True)
    running_now = models.BooleanField(default=False)
    alert = models.BooleanField(default=False)

    include = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    @property
    def last_run(self) -> datetime.datetime:
        history = self.taskevent_set.all().order_by("-started")
        if len(history) > 0:
            return history[0].started
        else:
            return datetime.datetime(year=1990, month=1, day=1)


    def dump_run_at_time(self, lst: List[datetime.time]) -> None:
        string_list = []
        for time in lst:
            string_list.append(time.strftime("%H:%M"))
        self.run_at_time = json.dumps(string_list)

    def load_run_at_time(self) -> List[datetime.time]:
        string_list = json.loads(self.run_at_time)
        times_list = []
        for time in string_list:
            times_list.append(datetime.datetime.strptime(time, "%H:%M"))
        return times_list



class TaskEvent(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    started = models.DateTimeField()
    finished = models.DateTimeField(null=True)
    success = models.BooleanField(default=True)

    log = models.TextField(default="")

    def __str__(self):
        return self.started.strftime("%H:%M") + " / " + self.task.name