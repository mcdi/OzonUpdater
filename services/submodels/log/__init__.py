from django.db import models

class Log(models.Model):
    short_error = models.CharField(max_length=2000)
    traceback = models.TextField()
    date_time = models.DateTimeField()

    def __str__(self):
        return self.short_error


