from services.config import TELEGRAM_CHAT_IDS
from services.config import BOT_TOKEN

import requests


class Alert:

    def send_message(self, text):
        responses = []
        for chat_id in TELEGRAM_CHAT_IDS:
            url = "https://api.telegram.org/bot{}/sendMessage".format(BOT_TOKEN)
            response = requests.get(url=url, params={"chat_id": chat_id,
                                                     "text": text})
            responses.append(response)
        return responses

    def send_document(self, message, file):
        responses = []
        for chat_id in TELEGRAM_CHAT_IDS:
            url = "https://api.telegram.org/bot{}/sendDocument".format(BOT_TOKEN)
            response = requests.post(
                url=url,
                data={"chat_id": chat_id, "caption": message},
                files={
                    "document": file
                }
            )
            responses.append(response)
        return responses
