from django.contrib import admin
from services.submodels.task import Task, TaskEvent
# Register your models here.


class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'run_every_mins',  'run_at_time', 'running_now', 'alert', 'include', )
    # list_display_links = ('', )
    list_filter = (('name', admin.ChoicesFieldListFilter,), )
    search_fields = (
        "name",
    )

admin.site.register(Task, TaskAdmin)

class TaskEventAdmin(admin.ModelAdmin):
    list_display = ('task', 'started',  'finished', 'success', )
    list_display_links = ('task', )
    list_filter = (('task__name', admin.ChoicesFieldListFilter,), )
    search_fields = (
        "task",
    )
admin.site.register(TaskEvent, TaskEventAdmin)