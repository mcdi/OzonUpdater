
from services.submodels.task import Task, TaskEvent
from services.utils.alert import Alert
import traceback
import datetime
import threading
import time as tm


class CronTask(object):

    RUN_EVERY_MINS = 0
    RUN_AT_TIME = []
    ALERT = False
    INCLUDE = False

    def __init__(self):
        self.db_task = self.registry()
        self.task_event: TaskEvent = None
        if self.is_time and self.INCLUDE and self.db_task.running_now==False:
            self.start_task()
            self.new_thread()

    def __repr__(self):
        return ""

    def run(self):
        try:
            self.do()
            self.finish_success_task()
        except Exception as e:
            short_error = e
            trace = traceback.format_exc()  # traceback
            self.finish_error_task(short_error, trace)

    def new_thread(self):
        new_thread = threading.Thread(target=self.run, args=())
        new_thread.start()


    def registry(self) -> Task:
        filter = Task.objects.filter(name=self.__class__.__name__)
        if len(filter) == 0:
            try:
                task = Task()
                task.name = self.__class__.__name__
                task.run_every_mins = self.RUN_EVERY_MINS
                task.alert = self.ALERT
                task.dump_run_at_time(self.RUN_AT_TIME)
                task.save()
                return task
            except Exception as e:
                return None
        else:
            try:
                task = filter[0]
                self.RUN_EVERY_MINS = task.run_every_mins
                self.ALERT = task.alert
                self.INCLUDE = task.include
                self.RUN_AT_TIME = task.load_run_at_time()
                return task
            except Exception as e:
                print(traceback.format_exc())
                return None



    def in_time_range(self, last_run):
        ERROR_TIME = 180 # Seconds

        dates_list = self.RUN_AT_TIME
        now = datetime.datetime.now()

        status = False
        for time in dates_list:
            need_run_datetime = datetime.datetime.now().replace(hour=time.hour, minute=time.minute, second=0)
            need_run_delta = (now - need_run_datetime).total_seconds()
            last_run_delta = (now - last_run).total_seconds()
            if need_run_delta > 0 and last_run_delta > ERROR_TIME and need_run_delta <= ERROR_TIME:
                status = True
        return status


    @property
    def is_time(self) -> bool:

        last_run = self.db_task.last_run.replace(tzinfo=None)
        now = datetime.datetime.now()
        delta = now - last_run
        if (delta.total_seconds() > self.RUN_EVERY_MINS * 60) and (self.RUN_EVERY_MINS != 0):
            return True
        elif self.in_time_range(last_run):
            return True
        else:
            return False


    def start_task(self):

        self.db_task.running_now = True
        self.db_task.save()

        task_event = TaskEvent()
        task_event.task = self.db_task
        task_event.started = datetime.datetime.now()
        task_event.save()
        self.task_event = task_event


    def finish_success_task(self):
        q = 1
        while True:
            try:
                self.db_task.running_now = False
                self.db_task.save()
                self.task_event.success = True
                self.task_event.finished = datetime.datetime.now()
                self.task_event.save()
                break
            except:
                tm.sleep(1)
                q += 1
                if q > 10:
                    break


    def finish_error_task(self, short_error, trace):

        q = 1
        while True:
            try:
                self.db_task.running_now = False
                self.db_task.save()
                self.task_event.success = False
                self.task_event.finished = datetime.datetime.now()
                self.task_event.log = "{}\n" \
                                      "*******************\n" \
                                      "{}".format(short_error, trace)
                self.task_event.save()

                if self.db_task.alert:
                    response = Alert().send_message(self.task_event.log)
                    # print(response)
                break
            except:
                tm.sleep(1)
                q += 1
                if q > 10:
                    break