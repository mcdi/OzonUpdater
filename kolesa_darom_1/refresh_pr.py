import json
import requests
import os


json_path = os.environ["BASE_DIR"] + "/" + "kolesa_darom_1/datas.json"
client_id = "40871"
api_key = "ead28228-a44f-489e-8031-86c4a05d5440"



with open(json_path, "r+") as json_file:
    json_data = json_file.read()

json_obj = json.loads(json_data)


def change_price(json_part, client_id, api_key):
    prices = []
    for goose in json_part:
        data_template = {
          "offer_id": goose["id"],
          "price": goose["price"]
        }
        prices.append(data_template)

    data = {"prices": prices}
    url = "http://api-seller.ozon.ru/v1/product/import/prices"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    # print(json.dumps(r.content.decode('utf-8')))


# Разбиваем по 1000
e1 = 0 +(len(json_obj))%1000
e2 = 999 + (len(json_obj))%1000

for i in range(0, len(json_obj)//1000+1):
    if i == 0:
        change_price(json_obj[0:(len(json_obj))%1000], client_id, api_key)
    json_part = json_obj[e1:e2]
    change_price(json_part, client_id, api_key)
    e1 += 1000
    e2 += 1000
