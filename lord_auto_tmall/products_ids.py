import top
import json



def get_product_list(product_ids, current_page, APP_KEY, SECRET, TOKEN, check_product_count=False, check_total_page=False):
    """If runs with two check flags only returns these check values and that's it."""
    request = top.api.AliexpressSolutionProductListGetRequest()
    request.set_app_info(top.appinfo(appkey=APP_KEY, secret=SECRET))
    request.aeop_a_e_product_list_query = json.dumps({'product_status_type': 'onSelling', #onSelling; offline; auditing; and editingRequired
                                                      'page_size': 200,
                                                      'current_page': current_page})
    response = request.getResponse(TOKEN)

    if not check_product_count and not check_total_page:
        product_ids.extend([item['product_id'] for item in response['aliexpress_solution_product_list_get_response']['result']['aeop_a_e_product_display_d_t_o_list']['item_display_dto']])
    else:
        product_count = response['aliexpress_solution_product_list_get_response']['result']['product_count']
        total_page = response['aliexpress_solution_product_list_get_response']['result']['total_page']
        return product_count, total_page


def get_product_info(product_id, product_ids, product_ids_map, APP_KEY, SECRET, TOKEN):
    try:
        request = top.api.AliexpressSolutionProductInfoGetRequest()
        request.set_app_info(top.appinfo(appkey=APP_KEY, secret=SECRET))
        request.product_id = product_id
        response = request.getResponse(TOKEN)
        sku_code = response['aliexpress_solution_product_info_get_response']['result']['aeop_ae_product_s_k_us']['global_aeop_ae_product_sku'][0]['sku_code']
        product_ids_map[product_id] = sku_code
    except Exception as e:
        pass
        # print(f"Product id {product_id} - ERROR! {str(e)}")


def refresh_json_with_products(path_to_json, APP_KEY, SECRET, TOKEN):
    """
    Function save to "path_to_json" json dict with mapping product_id:sku
    :param path_to_json: Output json file.
    :param APP_KEY: Ali app key.
    :param SECRET: Ali app secret.
    :param TOKEN: Shop token.
    """
    product_count, total_pages = get_product_list([], 1, APP_KEY, SECRET, TOKEN, check_product_count=True, check_total_page=True)
    product_ids = []
    product_ids_map = {}

    for current_page in range(1, total_pages + 1):

        get_product_list(product_ids, current_page, APP_KEY, SECRET, TOKEN)

    for product in enumerate(product_ids):
        number, product_id = product
        # print(number)
        get_product_info(product_id, product_ids, product_ids_map, APP_KEY, SECRET, TOKEN)

    with open(path_to_json, "w") as f:
        f.write(json.dumps(product_ids_map))