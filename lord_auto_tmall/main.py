import requests
import json
import re
import top


####### Операции с Xml фидом.
def download_xml(url, save_to_path):
    """ Download xml_feed """
    response = requests.get(url)
    with open(save_to_path, "wb") as f:
        f.write(response.content)


def get_data_from_xml(xml_feed_path):
    """
    Function parse xml file.
    :param xml_feed_path: path to xml feed
    :return: [(sku, price, stock), (sku, price, stock), ...]
    """
    with open(xml_feed_path, "r", encoding="windows-1251") as f:
        feed_text_data = f.read()
    datas = re.findall(r'Артикул=\"(.*?)\" Цена=\"(.*?)\" Остаток=\"(.*?)\"', feed_text_data, flags=re.DOTALL)
    return datas
########


########## Генераторы нужных json для методов Али
def refresh_stock_data_for_one_product(id, sku_list):
    """ Function generate data for ali refresh product method"""
    sku_data = []
    for sku in sku_list:
        sku_data.append({
            "sku_code": str(sku["sku"]),
            "inventory": int(float(sku["stock"]))
        })

    base_template = {
        "product_id": int(id),
        "multiple_sku_update_list": sku_data
    }
    return base_template


def refresh_price_data_for_one_product(id, sku_list):
    """ Function generate data for ali refresh product method"""
    sku_data = []
    for sku in sku_list:
        sku_data.append({
            "sku_code": str(sku["sku"]),
            "price": float(sku["price"])
        })

    base_template = {
        "product_id": int(id),
        "multiple_sku_update_list": sku_data
    }
    return base_template
#########


def refresh_stock(list_of_datas, APP_KEY, SECRET, TOKEN):
    """
    Обновление остатков(Для этого метода максимум 100 товаров
    Входные данные
    [{
        "id": "xxx",
        "sku_list": [
            {
                "sku": "xxx",
                "stock": "xxx"
            }
        ]
    },
    ... (Не более 100 в сумме)
    ]
    """
    req=top.api.AliexpressSolutionBatchProductInventoryUpdateRequest()
    req.set_app_info(top.appinfo(APP_KEY,SECRET))

    data = [refresh_stock_data_for_one_product(x["id"], x["sku_list"]) for x in list_of_datas]
    req.mutiple_product_update_list = data
    try:
        resp= req.getResponse(TOKEN)
        return resp
    except Exception as e:
        return e

def refresh_price(list_of_datas, APP_KEY, SECRET, TOKEN):
    """
    Обновление цен(Для этого метода максимум 100 товаров
    Входные данные
    [{
        "id": "xxx",
        "sku_list": [
            {
                "sku": "xxx",
                "price": "xxx"
            }
        ]
    },
    ... (Не более 100 в сумме)
    ]
    """
    req=top.api.AliexpressSolutionBatchProductPriceUpdateRequest()
    req.set_app_info(top.appinfo(APP_KEY,SECRET))

    data = [refresh_price_data_for_one_product(x["id"], x["sku_list"]) for x in list_of_datas]
    req.mutiple_product_update_list = data
    try:
        resp= req.getResponse(TOKEN)
        return resp
    except Exception as e:
        return e


def get_data_for_refresh_stocks(xml_feed_path, json_current_products_path):
    """
    Function makes filter for products in feed and connect to actually product_id
    :param xml_feed_path: local path to xml feed
    :param json_current_products_path: local path to dict.
    :return: [
        {
            "id": "***",
            "sku_list": [
                {
                    "sku": "***",
                    "stock": "***",
                    "price": "***"
                },
                ...,
                ...
            ]
        },
        ...,
        ...
    ]
    """

    with open(json_current_products_path, "r") as f:
        current_prosucts_dict = json.loads(f.read())

    datas = get_data_from_xml(xml_feed_path)
    refresh_input_list = []

    for key in current_prosucts_dict:
        data_filter = list(filter(lambda x:x[0]==current_prosucts_dict[key], datas))
        if len(data_filter) > 0:
            refresh_input_list.append({
                "id": key,
                "sku_list": [
                    {
                        "sku": current_prosucts_dict[key],
                        "stock": data_filter[0][2],
                        "price": data_filter[0][1]
                    }
                ]
            })
    return refresh_input_list


def save_responses(responses_json):
    with open("update_response.json", "w") as f:
        f.write(json.dumps(responses_json))


def update_stocks_and_prices_in_tmall(datas_list, APP_KEY, SECRET, TOKEN):
    length = len(datas_list)
    items, chunk = datas_list, 20

    split_stocks = list(zip(*[iter(items)] * chunk))
    remains_stocks = datas_list[length - length % 20: length]

    responses_json = []
    # Отправляем основную пачку заказов.
    for stock_list in split_stocks:
        for_send_stocks = list(stock_list)
        response = refresh_stock(for_send_stocks, APP_KEY, SECRET, TOKEN)
        responses_json.append(response)
        response = refresh_price(for_send_stocks, APP_KEY, SECRET, TOKEN)
        responses_json.append(response)

    # Добиваем остатки
    response = refresh_stock(remains_stocks, APP_KEY, SECRET, TOKEN)
    responses_json.append(response)
    response = refresh_price(remains_stocks, APP_KEY, SECRET, TOKEN)
    responses_json.append(response)
    save_responses(responses_json)