from services.cron import CronTask
from .main import download_xml, get_data_for_refresh_stocks, update_stocks_and_prices_in_tmall
from .products_ids import refresh_json_with_products
import os


XML_FEED_URL = "http://ftp113281.hostfx.ru/StockAli.xml"
XML_FEED_PATH = "xml.xml"
CURRENT_PRODUCTS_DICT = "ids.json"
APP_KEY = '30204124'
SECRET =  'e15b64bbcc3f059e3ab2306a696f43c9'
TOKEN = '50002801233p1x3zgugpqBvrgXiDPCrtufiEwgYFxwEcy1adc03caCswi8OXDVdgAkq'


class FileNotFoundError(Exception):
    pass


class LordTmallRefreshProductsDict(CronTask):
    RUN_EVERY_MINS = 1400
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        refresh_json_with_products(CURRENT_PRODUCTS_DICT, APP_KEY, SECRET, TOKEN)


class LordTmallRefreshProductsInTmall(CronTask):
    RUN_EVERY_MINS = 15
    RUN_AT_TIME = []
    INCLUDE = True
    ALERT = True

    # Your custom code
    def do(self):
        if not os.path.exists(CURRENT_PRODUCTS_DICT):
            raise FileNotFoundError(CURRENT_PRODUCTS_DICT)

        download_xml(XML_FEED_URL, XML_FEED_PATH)
        refresh_input_list = get_data_for_refresh_stocks(XML_FEED_PATH, CURRENT_PRODUCTS_DICT)
        update_stocks_and_prices_in_tmall(refresh_input_list, APP_KEY, SECRET, TOKEN)
