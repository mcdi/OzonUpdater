from pathlib import Path
import os
import xml.etree.ElementTree as ET
import requests
import json
from typing import List


def storage_path(cur_dir):
    """
    Function creates and returns absolute path to storage
    """
    storage_dir = os.path.join(cur_dir, "storage")
    if not os.path.isdir(storage_dir):
        os.mkdir(storage_dir)
    return storage_dir

def get_products_from_xml(xml_url, path_to_save):

    response = requests.get(xml_url)
    with open(path_to_save, "wb") as f:
        f.write(response.content)

    tree = ET.parse(path_to_save)
    root = tree.getroot()
    products = root.findall(".//offer")
    json_products = []
    for product in products:

        price = str(float(product.find("price").text))
        try:
            old_price = str(float(product.find("oldprice").text))
        except:
            old_price = price
        try:
            offer_id = str(product.find("vendorCode").text)
        except:
            continue
        try:
            stock = int(product.attrib["available"])
        except:
            stock = 0
        json_products.append({
            "offer_id": offer_id,
            "price": price,
            "old_price": old_price,
            "stock": stock
        })
    return json_products

def get_ozon_offer_ids_list(client_id, api_key) -> List[str]:
    offer_ids = []

    url = "http://api-seller.ozon.ru/v1/product/info/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    page_number = 1
    data = {
              "page": page_number,
              "page_size": 1000
            }

    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    total = json.loads(r.content.decode())["result"]["total"]
    for page_number in range(int(total/100)+1):
        data = {
                  "page": page_number + 1,
                  "page_size": 1000
                }

        r = requests.post(url=url, data=json.dumps(data), headers=headers)
        gooses = json.loads(r.content.decode())["result"]["items"]
        for goose in gooses:
            offer_ids.append(goose["offer_id"])
    return offer_ids

def change_available(datas_part, client_id, api_key):
    data = {"stocks": datas_part}
    url = "http://api-seller.ozon.ru/v1/product/import/stocks"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text

def change_price(datas_part, client_id, api_key):
    data = {"prices": datas_part}
    url = "http://api-seller.ozon.ru/v1/product/import/prices"
    headers = {
        "Client-Id": client_id,
        "Api-Key": api_key,
        "Content-Type": "application/json"
    }
    r = requests.post(url=url, json=data, headers=headers)
    return r.text

def array_division(array:list, division_by:int) -> list:
    """ Функция делит один большой список на несколькно малениких по division_by элементов в каждом """
    count = len(array)//division_by + 1
    final_list = []
    start:int = 0
    finish:int = 1
    while finish <= count:
        final_list.append(array[start*division_by : finish*division_by])
        start += 1
        finish += 1
    return final_list

def main():
    OZON_CLIENT_ID = "37552"
    OZON_API_KEY = "93ab7434-47c9-4803-9431-193a2441a9bc"

    CUR_DIR = str(Path(__file__).resolve(strict=True).parent)
    STORAGE_DIR = storage_path(CUR_DIR)

    xml_url = "https://kurazh-mebel.ru/upload/acrit.exportproplus/ozon.xml"
    xml_path = os.path.join(STORAGE_DIR, "feed.xml")
    final_json = get_products_from_xml(xml_url, xml_path)


    with open(os.path.join(STORAGE_DIR, "datas.json"), "w") as f:
        f.write(json.dumps(final_json))

    #### Update stocks in OZON
    chunks = array_division(final_json, 100)
    response = []
    for chunk in chunks:
        response_part = change_available(chunk, OZON_CLIENT_ID, OZON_API_KEY)
        response.append(response_part)

    with open(os.path.join(STORAGE_DIR, "response_av.json"), "w") as f:
        f.write(json.dumps(response))

    ### Update price in OZON
    chunks = array_division(final_json, 100)
    response = []
    for chunk in chunks:
        response_part = change_price(chunk, OZON_CLIENT_ID, OZON_API_KEY)
        response.append(response_part)

    with open(os.path.join(STORAGE_DIR, "response_pr.json"), "w") as f:
        f.write(json.dumps(response))

if __name__ == '__main__':
    main()
